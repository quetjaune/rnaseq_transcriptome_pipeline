#!/bin/env perl

$in_regexp = "[CLT]*"; #para indicar con que letra comienzan los archivos a procesar
$in_path   = "/LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/mapeo_bowtie2_all/";
$out_path  = "/LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/bam_converted/";


#crear directorio
mkdir $out_path;

my @files = <$in_path/*>;

foreach my $in_file (@files) {

   my $name = $in_file;
      $name =~ s/.+\///g;

   print "$name\n";
	my $out_file = "$out_path/$name";           
    
    system ("/LUSTRE/bioinformatica_data/genomica_funcional/bin/samtools-1.3.1/samtools view -S -b -o $out_file.bam $in_file");
       }
