#!/usr/bin/perl

use warnings;
use strict;
##my $bamPath = "mapeo_bowtie2_Marcos"; #para contar los que no estan concatenados.
my $bamPath = "/LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/sorted_bam/"; #activar cuando se kieren contar los archivos bam ya concatenados (paso 3)
#my $in_regexp = "[CLT]*";
my $logPath = "/LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/logs_sort";
my $inExt   = "bam";
my $outPath = "/LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/final_counts_all/";

my $cmd_arg = "/LUSTRE/bioinformatica_data/genomica_funcional/bin/samtools-1.3.1/samtools view";

my $host = `hostname`;
mkdir $logPath unless -e $logPath;
mkdir $outPath unless -e $outPath;

my @files = <$bamPath/*.$inExt>;

foreach my $in_file (@files) {

   my $name = $in_file;
      $name =~ s/.+\///g;

   print "$name\n";

   my $out_base = "$outPath/$name";
   # Make a logfile, decide if file has been run before
     
    my $logFile = "$logPath/$name.log";
          
   if (-e $logFile) {
      print "$logFile already exists, won't run again!\n";
      next;
    } else { 
      
             
        system ("$cmd_arg $in_file|perl /LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/scripts_4_Transcriptome/count_names.pl | gzip -c9 > $out_base.stats.gz"); # el ejecutable count_names.pl debe estar en la carpeta indicada
       
#print "$cmd_line\n";<STDIN>;next;
#system("$cmd_line && touch $logFile") == 0 && $subs++;
   }
}
