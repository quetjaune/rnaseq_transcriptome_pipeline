#install packages (sólo si es requerido)
# source("https://bioconductor.org/biocLite.R")
# biocLite("limma", type = "source")
# biocLite("edgeR", type = "source")


#limpiar todo
rm(list=ls())
theme_set(theme_bw())

# Differencial of Global Gene Expression Analysis.
library("limma")
library("edgeR")
library("ggplot2")
setwd("/home/marcos/Documents/RNAseq_Pulpo/")
outpathcount = "/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/"
dir.create(outpathcount, showWarnings=FALSE)
counts = read.table("tabla_counts.txt", header=TRUE, row.names = 1, sep="\t", comment.char="") 
head(counts)
dim(counts)

colnames(counts)[colnames(counts)=="CBL24POST.bam.sorted"] <- "CBL24POST"
colnames(counts)[colnames(counts)=="CBL24PRE.bam.sorted"] <- "CBL24PRE"
colnames(counts)[colnames(counts)=="CBL30POST.bam.sorted"] <- "CBL30POST"
colnames(counts)[colnames(counts)=="CBL30PRE.bam.sorted"] <- "CBL30PRE"
colnames(counts)[colnames(counts)=="LO24POST.bam.sorted"] <- "LO24POST"
colnames(counts)[colnames(counts)=="LO24PRE.bam.sorted"] <- "LO24PRE"
colnames(counts)[colnames(counts)=="LO30POST.bam.sorted"] <- "LO30POST"
colnames(counts)[colnames(counts)=="LO30PRE.bam.sorted"] <- "LO30PRE"
colnames(counts)[colnames(counts)=="T24POST.bam.sorted"] <- "T24POST"
colnames(counts)[colnames(counts)=="T24PRE.bam.sorted"] <- "T24PRE"
colnames(counts)[colnames(counts)=="T30POST.bam.sorted"] <- "T30POST"
colnames(counts)[colnames(counts)=="T30PRE.bam.sorted"] <- "T30PRE"
write.table(counts, file="/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/rawcounts.txt", row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
##raw analysis####
col_count_align = length(counts[counts[,ncol(counts)] > 0, ncol(counts)])

col9_count_align = length(counts[counts[,9] > 0, 9])

colsum = colSums(counts) 


###filtering and normalization#####

normalizados= cpm(counts)
lognormalizados=  log(normalizados)
write.table(lognormalizados, file= "/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/normalizados.txt", row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")

counts = counts[rowSums(cpm(counts) >= 3) >=3 ,]
dim(counts)

#genes_ensayados=rownames(counts)

counts =counts[,c(1:12)]
colnames(counts)
counts_noCBL30 = counts[,c(1:3,5:12)]
colnames(counts_noCBL30)
lo = grep("LO", colnames(counts))
counts_lo=counts[,lo]
colnames(counts_lo)


grp = sub("LO24P[ORSTE]", "LO24", colnames(counts_lo))


# write.table(dge$samples, file= "/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/DGElist_samples.txt", row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
# write.table(dge$counts, file= "/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/DGElist_counts.txt", row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")

#MDS para observar la dispersion de los datos:
#tiff(filename = "MDSplot_Lth_dim21.tiff", width = 768, height = 1024, units = "px", pointsize = 16, compression = c("lzw"),
    #bg = "white", res = NA)
pdf("Analisis_Diff_Expr_R/MDSplot_noCBL30PRE.pdf")
cols <- rep(c("red","blue","green","black", "yellow", "brown", "cyan", "magenta", "gray", "orange", "purple"), each = 1)
plotMDS(counts_noCBL30, top=500, col=cols, xlab="Dim1", ylab="Dim2", dim.plot=c(1,2))
abline(h= c(1,-1),  col="blue", lwd=1, lty=2)
abline(v= c(1,-1),  col="blue", lwd=1, lty=2)
dev.off()

#grouped by tissue
#LOBULO OPTICO (LO)
lo = grep("LO", colnames(counts))
counts_lo=counts[,lo]
pdf("Analisis_Diff_Expr_R/MDSplot_onlyLO.pdf")
cols <- rep(c("red","blue","green","black"), each = 1)
plotMDS(counts_lo, top=500, col=cols, xlab="Dim1", ylab="Dim2", dim.plot=c(1,2))
abline(h= c(1,-1),  col="blue", lwd=1, lty=2)
abline(v= c(1,-1),  col="blue", lwd=1, lty=2)
dev.off()

#CBL (CBL)
cbl = grep("CBL", colnames(counts))
counts_cbl=counts[,cbl]
pdf("Analisis_Diff_Expr_R/MDSplot_onlyCBL.pdf")
cols <- rep(c("red","blue","green","black"), each = 1)
plotMDS(counts_cbl, top=500, col=cols, xlab="Dim1", ylab="Dim2", dim.plot=c(1,2))
abline(h= c(1,-1),  col="blue", lwd=1, lty=2)
abline(v= c(1,-1),  col="blue", lwd=1, lty=2)
dev.off()

#Testiculo (T)
t = grep("^[T]", colnames(counts))
counts_t=counts[,t]
pdf("Analisis_Diff_Expr_R/MDSplot_onlyT.pdf")
cols <- rep(c("red","blue","green","black"), each = 1)
plotMDS(counts_t, top=500, col=cols, xlab="Dim1", ylab="Dim2", dim.plot=c(1,2))
abline(h= c(1,-1),  col="blue", lwd=1, lty=2)
abline(v= c(1,-1),  col="blue", lwd=1, lty=2)
dev.off()

d1=counts_lo[,c(1:2)]
d1=d
rownames(match(1, d1))
#MDS 3D
# library(rgl)
#tiff(filename = "MDS_Lth_3D", width = 768, height = 1024, units = "px", pointsize = 12, compression = c("lzw"),
   # bg = "white", res = NA)
# cols <- rep(c("red","blue","green","black"), each = 3, BackColor="white")
# mds <- plotMDS(dge, top=500, col=cols, ndim=3, xlab="Dim1", ylab="Dim2")
# x= mds$cmdscale.out[,1]
# y=mds$cmdscale.out[,2]
# z=mds$cmdscale.out[,3]
# plot3d(x,y,z, col=cols)
# rgl.postscript("3d_MDS_dim1.pdf","pdf")
# dev.off()
# plot3d(y,x,z, col=cols)
# rgl.postscript("3d_MDS_dim2.pdf","pdf")
# dev.off()
# plot3d(z,y,x, col=cols)
# rgl.postscript("3d_MDS_dim3.pdf","pdf")
# abline(h= c(1,-1), col="blue", lwd=1, lty=2)
# dev.off()
#MDS_3D_rotation_images
# rgl.open()
# plot3d(x,y,z, col=cols, type='s', size= 5)
# rgl.postscript("3d_MDS_esf4.pdf","pdf")
# dev.off()
# #rgl.viewpoint(0,20)
# #for (i in 1:45) { rgl.viewpoint(i,20) 
#                   filename <- paste("mds",formatC(i,digits=1,flag="0"),".eps",sep="") 
#                   rgl.postscript(filename, fmt="eps")}


colnames(counts)

###################################################################################
#EXISTEN 2 opciones para el análisis: (1) asignar un valor arbitrario de dispersión 
#(de acuerdo a la dispersión observada en experimentos similares) o (2) usar pseudoréplicas
###################################################################################


#(1) asignando valor de dispersión arbitrario = 0.1, recomendado en manual de edgeR, en caso de no tener replicas


bcv <- 0.1
# counts <- matrix( rnbinom(40,size=1/bcv^2,mu=10), 20,2)
y <- DGEList(counts=counts[,c(5,7)], group=1:2) #aqui se define que columnas se van a comparar en el test estadístico
# y = estimateGLMCommonDisp(y, method = "deviance", subset = NULL)
# y = estimateGLMTrendedDisp(y)
# y = estimateGLMTagwiseDisp(y)


head(y$counts)

et <- exactTest(y, dispersion=bcv^2)
et <- glmFit(y, dispersion=bcv^2)

head(sort(et$table$PValue))

# for (comp in colnames(counts)) {
#   print(comp)
#   
#   lrt <- glmLRT(y, contrast=c("L024POST-LO30POST"))
#   
#   topTab <- topTags(lrt, n=Inf)$table

deGenes <- rownames(et)[et$table$PValue < 0.01] #& abs(topTab$logFC) > 0]
deGenes_up <- rownames(et)[et$table$PValue < 0.01 & (et$table$logFC) > 2]
deGenes_down <- rownames(et)[et$table$PValue < 0.01 & (et$table$logFC) < 2]

print (length(deGenes))
print (length(deGenes_up))
print (length(deGenes_down))

# tabFile <- paste(comp, ".txt", sep="")
write.table(deGenes, file="deg_LOPOST24-30.txt", sep="\t", quote=FALSE)
# tabFile <- paste(comp, "_up.txt", sep="")
write.table(deGenes_up, file="deg_up_LOPOST24-30.txt", sep="\t", quote=FALSE)
# tabFile <- paste(comp, "_down.txt", sep="")
write.table(deGenes_down, file="deg_down_LOPOST24-30.txt", sep="\t", quote=FALSE)

#Utilizar el ejemplo anterior para adaptar a las comparaciones por pares que se deseen hacer

###############################################################################

#(2) diseño experimental usando peudoreplicas.

#################################################################################
################################################################################
#pseudoreplicas 30 vs 24
###############################################################################
################################################################################

# design
counts =counts[,c(1:12)]
grp = sub("POST|PRE", "", colnames(counts))
grp
dge = DGEList(counts=counts, group=grp) 
dge

write.table(dge$samples, file= "/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/DGElist_samples.txt", row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
write.table(dge$counts, file= "/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/DGElist_counts.txt", row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")

#para calcular el factor de normalizacion.
dge = calcNormFactors(dge) 
dge

#diseño experimental (matriz con todas las cepas) usando a las que tienen el mismo tratamiento como replica.

design = model.matrix(~0 + dge$samples$group)
colnames(design) = levels(dge$samples$group)
design

#calculando la dispersion de los datos.
dge = estimateGLMCommonDisp(dge, design = design)
dge = estimateGLMTrendedDisp(dge, design = design)
dge = estimateGLMTagwiseDisp(dge, design = design)

tiff(filename = "/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/plotBCV.tiff", width = 768, height = 1024, units = "px", pointsize = 12, compression = c("lzw"),
     bg = "white", res = NA)
plotBCV(dge)
dev.off()

#imprimir dispersion
print(dge$common.dispersion)
common.dispersion=dge$common.dispersion
write.table(common.dispersion, file="/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/commondispersion.txt", sep="", row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
write.table(dge, file="/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/dge.txt", row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
################################################################
#fit usando la dispersion calculada
fit=glmFit(dge, design) 

colnames(fit)

# makeContrasts

contVector <- c(
  "CBL_30vs24"= "CBL30-CBL24",
  "LO_30vs24"= "LO30-LO24",
  "T_30vs24"="T30-T24"
  # "GW_noHS_vs_VS_noHS"= "GW_noHS-VS_noHS"
  #"GW_HS_resp_vs_VS_HS_resp"="(GW_HS-GW_noHS)-(VS_HS-VS_noHS)"
    )
contMatrix <- makeContrasts(contrasts=contVector, levels=design)
colnames(contMatrix) <- names(contVector)

#comparacion todos vs todos

for (comp in colnames(contMatrix)) {
  print(comp)

  lrt <- glmLRT(fit, contrast=contMatrix[,comp])

  topTab <- topTags(lrt, n=Inf)$table

  deGenes <- rownames(topTab)[topTab$FDR < 0.1] #& abs(topTab$logFC) > 0]
  deGenes_up <- rownames(topTab)[topTab$FDR < 0.1 & (topTab$logFC) > 0]
  deGenes_down <- rownames(topTab)[topTab$FDR < 0.1 & (topTab$logFC) < 0]

  print (length(deGenes))
  print (length(deGenes_up))
  print (length(deGenes_down))


  tiff(filename = paste(outpathcount, comp, ".tiff", sep=""), width = 768, height = 1024, units = "px", pointsize = 12, compression = c("lzw"),
    bg = "white", res = NA)
  plotSmear(lrt, de.tags=deGenes, main=comp)
  abline(h= c(1,-1), col="blue", lwd=1, lty=2)
  dev.off()

  # tiff(filename = paste(outpathcount, comp, "volcano.tiff", sep=""), width = 768, height = 1024, units = "px", pointsize = 12, compression = c("lzw"),
  #    bg = "white", res = NA)
  # volcanoplot(lrt, de.tags=deGenes, main=comp)
  # abline(h= c(1,-1), col="blue", lwd=1, lty=2)
  # dev.off()


  # pdf(paste(comp, ".pdf")
  # plotSmear(lrt, paste(comp, ".jpeg", sep=""), de.tags=deGenes, ylab= paste(comp, "logFC"))
  # dev.off()
  # 


  write.table(topTab, file=paste(outpathcount, comp, ".txt", sep=""), row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
  write.table(deGenes_up, file=paste(outpathcount, comp, "_up.txt", sep=""), row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
  write.table(deGenes_down, file=paste(outpathcount, comp, "_down.txt", sep=""), row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")

  tabFile <- paste(comp, ".txt", sep="")
  write.table(topTab, file=tabFile, sep="\t", quote=FALSE)
  tabFile <- paste(comp, "_up.txt", sep="")
  write.table(deGenes_up, file=tabFile, sep="\t", quote=FALSE)
  tabFile <- paste(comp, "_down.txt", sep="")
  write.table(deGenes_down, file=tabFile, sep="\t", quote=FALSE)
}

#para construir el heatmap

library(RColorBrewer)

DiffExpFuncByGLM<-function(contrasts,fitted,cutoffFDR,cutoffLFC)
{
  WholeDgeGenes<-NULL;topTab<-NULL;dgeGenes<-NULL;lrt<-NULL
  for(i in 1:ncol(contMatrix))
  {
    
    lrt <- glmLRT(fit, contrast=contMatrix[,colnames(contMatrix)[i]])
    topTab=topTags(lrt, n=Inf)$table
    
    WholeDgeGenes = append(WholeDgeGenes,rownames(topTab)[topTab$FDR < cutoffFDR & abs (topTab$logFC)>cutoffLFC] )
  }
  UniqueDEG<-unique(WholeDgeGenes)
  DEGMatrixLFC<-matrix(0,length(UniqueDEG),ncol(contMatrix))
  colnames(DEGMatrixLFC)<-colnames(contMatrix)
  rownames(DEGMatrixLFC)<-UniqueDEG
  
  DEGMatrixFDR<-matrix(0,length(UniqueDEG),ncol(contMatrix))
  colnames(DEGMatrixFDR)<-colnames(contMatrix)
  rownames(DEGMatrixFDR)<-UniqueDEG
  
  WholeDgeGenes<-NULL;topTab<-NULL;dgeGenes<-NULL;lrt<-NULL
  for(i in 1:ncol(contMatrix))
  {
    lrt <- glmLRT(fit, contrast=contMatrix[,colnames(contMatrix)[i]])
    topTab=topTags(lrt, n=Inf)$table
    DEGMatrixLFC[,colnames(contMatrix)[i]]<-topTab[UniqueDEG,]$logFC
    DEGMatrixFDR[,colnames(contMatrix)[i]]<-topTab[UniqueDEG,]$FDR
  }
  return(list(DEGMatrixLFC,DEGMatrixFDR))
}


DiffExp.CutAbs0.0<-DiffExpFuncByGLM(contrasts=contMatrix,fitted=fit,cutoffFDR=0.01,cutoffLFC=0)

DEGMatrixLFC<-DiffExp.CutAbs0.0[[1]]
DEGMatrixFDR<-DiffExp.CutAbs0.0[[2]]
LFC.D.E.A<-DEGMatrixLFC###Selecciona las columnas que te interesan. Si quiere toda mejor usa el DEGMatrixLFC
LFC.DExppToHeatmap<-LFC.D.E.A

colors = c(seq(-5,-0.5,length=100),seq(-0.5,0.5,length=100),seq(0.5,5,length=100))
my_palette <- colorRampPalette(c("red", "black", "green"))(n = 299)
hr=hclust(dist(LFC.DExppToHeatmap), method="complete")
tiff(filename = "/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/Diff.Expression.30vs24.tiff",width=1100,height = 1800,
     units = "px", compression = c("lzw"),pointsize = 14,bg = "white")  #
heatmap(LFC.DExppToHeatmap, Rowv=as.dendrogram(hr), Colv=TRUE, col=my_palette, breaks=colors, labRow=FALSE, cexCol=1, margin=c(10,15))
dev.off()

#######################################################################
#######################################################################
#PRUEBA CON pseudoreplicas POSTvsPRE
###################################################################
########################################################################

# design
counts =counts[,c(1:12)]
grp = sub("30|24", "", colnames(counts))
grp
dge = DGEList(counts=counts, group=grp) 
dge

write.table(dge$samples, file= "/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/DGElist_samples_postvspre.txt", row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
write.table(dge$counts, file= "/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/DGElist_counts_postvspre.txt", row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")

#para calcular el factor de normalizacion.
dge = calcNormFactors(dge) 
dge

#diseño experimental (matriz con todas las muestras) usando a las que tienen el mismo tratamiento como replica.

design = model.matrix(~0 + dge$samples$group)
colnames(design) = levels(dge$samples$group)
design

#calculando la dispersion de los datos.
dge = estimateGLMCommonDisp(dge, design = design)
dge = estimateGLMTrendedDisp(dge, design = design)
dge = estimateGLMTagwiseDisp(dge, design = design)

tiff(filename = "/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/plotBCV_postvspre.tiff", width = 768, height = 1024, units = "px", pointsize = 12, compression = c("lzw"),
     bg = "white", res = NA)
plotBCV(dge)
dev.off()

#imprimir dispersion
print(dge$common.dispersion)
common.dispersion=dge$common.dispersion
write.table(common.dispersion, file="/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/commondispersion_postvspre.txt", sep="", row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
write.table(dge, file="/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/dge_postvspre.txt", row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
################################################################
#fit usando la dispersion calculada
fit=glmFit(dge, design) 

colnames(fit)

# makeContrasts

contVector <- c(
  "CBL_POSTvsPRE"= "CBLPOST-CBLPRE",
  "LO_POSTvsPRE"= "LOPOST-LOPRE",
  "T_POSTvsPRE"="TPOST-TPRE"
  # "GW_noHS_vs_VS_noHS"= "GW_noHS-VS_noHS"
  #"GW_HS_resp_vs_VS_HS_resp"="(GW_HS-GW_noHS)-(VS_HS-VS_noHS)"
)
contMatrix <- makeContrasts(contrasts=contVector, levels=design)
colnames(contMatrix) <- names(contVector)

#comparacion todos vs todos

for (comp in colnames(contMatrix)) {
  print(comp)
  
  lrt <- glmLRT(fit, contrast=contMatrix[,comp])
  
  topTab <- topTags(lrt, n=Inf)$table
  
  deGenes <- rownames(topTab)[topTab$FDR < 0.1] #& abs(topTab$logFC) > 0]
  deGenes_up <- rownames(topTab)[topTab$FDR < 0.1 & (topTab$logFC) > 0]
  deGenes_down <- rownames(topTab)[topTab$FDR < 0.1 & (topTab$logFC) < 0]
  
  print (length(deGenes))
  print (length(deGenes_up))
  print (length(deGenes_down))
  
  
  tiff(filename = paste(outpathcount, comp, ".tiff", sep=""), width = 768, height = 1024, units = "px", pointsize = 12, compression = c("lzw"),
       bg = "white", res = NA)
  plotSmear(lrt, de.tags=deGenes, main=comp)
  abline(h= c(1,-1), col="blue", lwd=1, lty=2)
  dev.off()
  
  # tiff(filename = paste(outpathcount, comp, "volcano.tiff", sep=""), width = 768, height = 1024, units = "px", pointsize = 12, compression = c("lzw"),
  #    bg = "white", res = NA)
  # volcanoplot(lrt, de.tags=deGenes, main=comp)
  # abline(h= c(1,-1), col="blue", lwd=1, lty=2)
  # dev.off()
  
  
  # pdf(paste(comp, ".pdf")
  # plotSmear(lrt, paste(comp, ".jpeg", sep=""), de.tags=deGenes, ylab= paste(comp, "logFC"))
  # dev.off()
  # 
  
  
  write.table(topTab, file=paste(outpathcount, comp, ".txt", sep=""), row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
  write.table(deGenes_up, file=paste(outpathcount, comp, "_up.txt", sep=""), row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
  write.table(deGenes_down, file=paste(outpathcount, comp, "_down.txt", sep=""), row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
  
  tabFile <- paste(comp, ".txt", sep="")
  write.table(topTab, file=tabFile, sep="\t", quote=FALSE)
  tabFile <- paste(comp, "_up.txt", sep="")
  write.table(deGenes_up, file=tabFile, sep="\t", quote=FALSE)
  tabFile <- paste(comp, "_down.txt", sep="")
  write.table(deGenes_down, file=tabFile, sep="\t", quote=FALSE)
}

#para construir el heatmap

library(RColorBrewer)

DiffExpFuncByGLM<-function(contrasts,fitted,cutoffFDR,cutoffLFC)
{
  WholeDgeGenes<-NULL;topTab<-NULL;dgeGenes<-NULL;lrt<-NULL
  for(i in 1:ncol(contMatrix))
  {
    
    lrt <- glmLRT(fit, contrast=contMatrix[,colnames(contMatrix)[i]])
    topTab=topTags(lrt, n=Inf)$table
    
    WholeDgeGenes = append(WholeDgeGenes,rownames(topTab)[topTab$FDR < cutoffFDR & abs (topTab$logFC)>cutoffLFC] )
  }
  UniqueDEG<-unique(WholeDgeGenes)
  DEGMatrixLFC<-matrix(0,length(UniqueDEG),ncol(contMatrix))
  colnames(DEGMatrixLFC)<-colnames(contMatrix)
  rownames(DEGMatrixLFC)<-UniqueDEG
  
  DEGMatrixFDR<-matrix(0,length(UniqueDEG),ncol(contMatrix))
  colnames(DEGMatrixFDR)<-colnames(contMatrix)
  rownames(DEGMatrixFDR)<-UniqueDEG
  
  WholeDgeGenes<-NULL;topTab<-NULL;dgeGenes<-NULL;lrt<-NULL
  for(i in 1:ncol(contMatrix))
  {
    lrt <- glmLRT(fit, contrast=contMatrix[,colnames(contMatrix)[i]])
    topTab=topTags(lrt, n=Inf)$table
    DEGMatrixLFC[,colnames(contMatrix)[i]]<-topTab[UniqueDEG,]$logFC
    DEGMatrixFDR[,colnames(contMatrix)[i]]<-topTab[UniqueDEG,]$FDR
  }
  return(list(DEGMatrixLFC,DEGMatrixFDR))
}


DiffExp.CutAbs0.0<-DiffExpFuncByGLM(contrasts=contMatrix,fitted=fit,cutoffFDR=0.01,cutoffLFC=0)

DEGMatrixLFC<-DiffExp.CutAbs0.0[[1]]
DEGMatrixFDR<-DiffExp.CutAbs0.0[[2]]
LFC.D.E.A<-DEGMatrixLFC###Selecciona las columnas que te interesan. Si quiere toda mejor usa el DEGMatrixLFC
LFC.DExppToHeatmap<-LFC.D.E.A

colors = c(seq(-6,-0.5,length=100),seq(-0.5,0.5,length=100),seq(0.5,6,length=100))
my_palette <- colorRampPalette(c("red", "black", "green"))(n = 299)
hr=hclust(dist(LFC.DExppToHeatmap), method="complete")
tiff(filename = "/home/marcos/Documents/RNAseq_Pulpo/Analisis_Diff_Expr_R/Diff.Expression.POSTvsPRE.tiff",width=1100,height = 1800,
     units = "px", compression = c("lzw"),pointsize = 14,bg = "white")  #
heatmap(LFC.DExppToHeatmap, Rowv=as.dendrogram(hr), Colv=TRUE, col=my_palette, breaks= colors, labRow=FALSE, cexCol=1, margin=c(10,15)) 
+ theme(legend())
+ guide_legend(waiver(), keywidth = 0.5, keyheight = 0.9, direction = "vertical")
dev.off()

########################################################################
########################################################################

#Para seleccionar clusters

library (RColorBrewer)
tabla2 <- data.matrix(LFC.DExppToHeatmap)#debe ser una matriz
pair.break <- seq(-7, 7, by=0.5)
mycol <- colorpanel(n=28, low="green", mid="black", high="red")

#para hacer el heatmap
hr <- hclust(as.dist(1 - cor(t(tabla2), method="pearson")), method="complete")#existen varios modelos de clusterizacion, los mas usados son pearson y spearman
hc <- hclust(as.dist(1 - cor(tabla2, method="pearson")), method="complete")
#para visualizar los dendogramas de las clusterizaciones:
plot(as.dendrogram(hr))
dev.off()
plot(as.dendrogram(hc))
dev.off()
mycl  <- cutree(hr, h=max(hr$height)/1.5); mycolhc <-  rainbow(length(unique(mycl)), 
                                                               start=0.1, end=0.9); mycolhc <-  mycolhc[as.vector(mycl)]

#HACIENDO EL HEATMAP PRINCIPAL###
tiff(filename="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/heatmap_pearson_clusters.tiff", width=1400,height = 2100,
   units = "px", compression = c("lzw"),pointsize =16,bg = "white")
#jpeg("/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/Lth_HSresp_clusters_spearman.jpeg", quality=80)
heatmap.2(tabla2, Rowv=as.dendrogram(hr), Colv=FALSE, labRow=FALSE, dendrogram="row", col=mycol[1:28], breaks=pair.break[1:29], key=TRUE, trace="none", RowSideColors=mycolhc, cexRow=0.6, cexCol=0.9, margin=c(5, 25), densadj=0.2, keysize=0.7, density.info=c("none") )
dev.off()
##########################################################
#####para graficar subgrupos del dendograma####

#con esta grafica puesdes escoger el cluster que kieres visualisar.

names(mycolhc) <- names(mycl); barplot(rep(10, max(mycl)), 
                                       col=unique(mycolhc[hr$labels[hr$order]]), horiz=T,  names=unique(mycl[hr$order]))


#aqui se selige que cluster kieres graficas, 1,3 o la combinacion que kieras.
clid  <- c(1); ysub <- tabla2[names(mycl[mycl%in%clid]),]; 
hrsub <-  hclust(as.dist(1-cor(t(ysub), method="pearson")), method="complete")

#hacer la grafica del heatmap

#tiff(filename="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster1_Lth_cont4.tiff", width=1200,height = 1900,
  #   units = "px", compression = c("lzw"),pointsize = 14,bg = "white")
pdf("/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster1_Lth_cont4.pdf")
heatmap.2(ysub, Rowv=as.dendrogram(hrsub), Colv="FALSE", dendrogram="row", col=mycol[1:28], breaks=pair.break[1:29], trace="none", RowSideColors=mycolhc[mycl%in%clid] , cexRow=0.7, cexCol=0.3, keysize=1.2, densadj=1.5)
dev.off()

#para sacar la lista de ysub(ids de los genes que constituyen en cluster)
write.table(ysub, file="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster1_Lth_cont4.txt", sep="\t")

#CLUSTER2
#aqui se selige que cluster kieres graficas, 1,3 o la combinacion que kieras.
clid  <- c(2); ysub <- tabla2[names(mycl[mycl%in%clid]),]; 
hrsub <-  hclust(as.dist(2-cor(t(ysub), method="pearson")), method="complete")

#hacer la grafica del heatmap

#tiff(filename="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster1_Lth_cont4.tiff", width=1200,height = 1900,
#   units = "px", compression = c("lzw"),pointsize = 14,bg = "white")
pdf("/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster2_Lth_cont4.pdf")
heatmap.2(ysub, Rowv=as.dendrogram(hrsub), Colv="FALSE", dendrogram="row", col=mycol[1:28], breaks=pair.break[1:29], trace="none", RowSideColors=mycolhc[mycl%in%clid] , cexRow=0.7, cexCol=0.3, keysize=1.2, densadj=1.5)
dev.off()

#para sacar la lista de ysub(ids de los genes que constituyen en cluster)
write.table(ysub, file="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster2_Lth_cont4.txt", sep="\t")

#CLUSTER3
#aqui se selige que cluster kieres graficas, 1,3 o la combinacion que kieras.
clid  <- c(3); ysub <- tabla2[names(mycl[mycl%in%clid]),]; 
hrsub <-  hclust(as.dist(3-cor(t(ysub), method="pearson")), method="complete")

#hacer la grafica del heatmap

#tiff(filename="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster1_Lth_cont4.tiff", width=1200,height = 1900,
#   units = "px", compression = c("lzw"),pointsize = 14,bg = "white")
pdf("/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster3_Lth_cont4.pdf")
heatmap.2(ysub, Rowv=as.dendrogram(hrsub), Colv="FALSE", dendrogram="row", col=mycol[1:28], breaks=pair.break[1:29], trace="none", RowSideColors=mycolhc[mycl%in%clid] , cexRow=0.7, cexCol=0.3, keysize=1.2, densadj=1.5)
dev.off()

#para sacar la lista de ysub(ids de los genes que constituyen en cluster)
write.table(ysub, file="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster3_Lth_cont4.txt", sep="\t")

#CLUSTER4
#aqui se selige que cluster kieres graficas, 1,3 o la combinacion que kieras.
clid  <- c(4); ysub <- tabla2[names(mycl[mycl%in%clid]),]; 
hrsub <-  hclust(as.dist(4-cor(t(ysub), method="pearson")), method="complete")

#hacer la grafica del heatmap

#tiff(filename="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster1_Lth_cont4.tiff", width=1200,height = 1900,
#   units = "px", compression = c("lzw"),pointsize = 14,bg = "white")
pdf("/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster4_Lth_cont4.pdf")
heatmap.2(ysub, Rowv=as.dendrogram(hrsub), Colv="FALSE", dendrogram="row", col=mycol[1:28], breaks=pair.break[1:29], trace="none", RowSideColors=mycolhc[mycl%in%clid] , cexRow=0.7, cexCol=0., keysize=1.2, densadj=1.5)
dev.off()

#para sacar la lista de ysub(ids de los genes que constituyen en cluster)
write.table(ysub, file="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster4_Lth_cont4.txt", sep="\t")

#CLUSTER5
#aqui se selige que cluster kieres graficas, 1,3 o la combinacion que kieras.
clid  <- c(5); ysub <- tabla2[names(mycl[mycl%in%clid]),]; 
hrsub <-  hclust(as.dist(5-cor(t(ysub), method="pearson")), method="complete")

#hacer la grafica del heatmap

#tiff(filename="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster1_Lth_cont4.tiff", width=1200,height = 1900,
#   units = "px", compression = c("lzw"),pointsize = 14,bg = "white")
pdf("/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster5_Lth_cont4.pdf")
heatmap.2(ysub, Rowv=as.dendrogram(hrsub), Colv="FALSE", dendrogram="row", col=mycol[1:28], breaks=pair.break[1:29], trace="none", RowSideColors=mycolhc[mycl%in%clid] , cexRow=0.7, cexCol=0.3, keysize=1.2, densadj=1.5)
dev.off()

#para sacar la lista de ysub(ids de los genes que constituyen en cluster)
write.table(ysub, file="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster5_Lth_cont4.txt", sep="\t")

#CLUSTER6
#aqui se selige que cluster kieres graficas, 1,3 o la combinacion que kieras.
clid  <- c(6); ysub <- tabla2[names(mycl[mycl%in%clid]),]; 
hrsub <-  hclust(as.dist(6-cor(t(ysub), method="pearson")), method="complete")

#hacer la grafica del heatmap

#tiff(filename="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster1_Lth_cont4.tiff", width=1200,height = 1900,
#   units = "px", compression = c("lzw"),pointsize = 14,bg = "white")
pdf("/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster6_Lth_cont4.pdf")
heatmap.2(ysub, Rowv=as.dendrogram(hrsub), Colv="FALSE", dendrogram="row", col=mycol[1:28], breaks=pair.break[1:29], trace="none", RowSideColors=mycolhc[mycl%in%clid] , cexRow=0.7, cexCol=0.3, keysize=1.2, densadj=1.5)
dev.off()

#para sacar la lista de ysub(ids de los genes que constituyen en cluster)
write.table(ysub, file="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster6_Lth_cont4.txt", sep="\t")
#CLUSTER7
#aqui se selige que cluster kieres graficas, 1,3 o la combinacion que kieras.
clid  <- c(7); ysub <- tabla2[names(mycl[mycl%in%clid]),]; 
hrsub <-  hclust(as.dist(7-cor(t(ysub), method="pearson")), method="complete")

#hacer la grafica del heatmap

#tiff(filename="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster1_Lth_cont4.tiff", width=1200,height = 1900,
#   units = "px", compression = c("lzw"),pointsize = 14,bg = "white")
pdf("/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster7_Lth_cont4.pdf")
heatmap.2(ysub, Rowv=as.dendrogram(hrsub), Colv="FALSE", dendrogram="row", col=mycol[1:28], breaks=pair.break[1:29], trace="none", RowSideColors=mycolhc[mycl%in%clid] , cexRow=0.7, cexCol=0.3, keysize=1.2, densadj=1.5)
dev.off()

#para sacar la lista de ysub(ids de los genes que constituyen en cluster)
write.table(ysub, file="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster7_Lth_cont4.txt", sep="\t")
#CLUSTER8
#aqui se selige que cluster kieres graficas, 1,3 o la combinacion que kieras.
clid  <- c(8); ysub <- tabla2[names(mycl[mycl%in%clid]),]; 
hrsub <-  hclust(as.dist(8-cor(t(ysub), method="pearson")), method="complete")

#hacer la grafica del heatmap

#tiff(filename="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster1_Lth_cont4.tiff", width=1200,height = 1900,
#   units = "px", compression = c("lzw"),pointsize = 14,bg = "white")
pdf("/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster8_Lth_cont4.pdf")
heatmap.2(ysub, Rowv=as.dendrogram(hrsub), Colv="FALSE", dendrogram="row", col=mycol[1:28], breaks=pair.break[1:29], trace="none", RowSideColors=mycolhc[mycl%in%clid] , cexRow=0.7, cexCol=0.5, keysize=1.2, densadj=1.5)
dev.off()

#para sacar la lista de ysub(ids de los genes que constituyen en cluster)
write.table(ysub, file="/home/marcos/Documents/Transcriptome/Analisis_Diff_Expr_R/long_isof_assembly/clusters/cluster8_Lth_cont4.txt", sep="\t")