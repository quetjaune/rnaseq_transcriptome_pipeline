#!/bin/bash
### Seleccionar la particion (cola) de ejecucion
#SBATCH -p cicese

### Nombre de la tarea
#SBATCH --job-name bam_conv

### Num de procesos
##SBATCH -n 24

## Cantidad de nodos
#SBATCH -N 1

#SBATCH -o bam_sam.job
#SBATCH -e bam_sam_error.job

cd ${SLURM_SUBMIT_DIR}
cd /LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/
perl scripts_4_Transcriptome/4_sam_bam_conversion.pl 