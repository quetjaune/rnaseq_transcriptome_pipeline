#!/bin/env perl

#para construir el index
#system ( "bowtie2-build /LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/Transcriptoma_completo.fasta /LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/Index_bowtie2/bt2_index");

$ebwt_file = "/LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/Index_bowtie2/bt2_index";
$in_regexp = "[CLT]*"; #para indicar con que letra comienzan los archivos a procesar
$in_path   = "/LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/Fastq_New_names/";
$out_path  = "/LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/mapeo_bowtie2_all/";
$log_path = "/LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/logs_bowtie2_all/";

# lee todos los nombres de archevo que terminan en .gz
my $pdata_file = "names_all.txt"; #este archivo indica el directorio y nombre de los archivos (en mi caso para cada muestra/tratamiento) que se pretenden procesar. Se usa un nombre por línea.
chomp(@in_files = `tail -n +1 $pdata_file`);

# crea los directorios
mkdir $out_path;
mkdir $log_path;

#usar sólo si se corre en servidor####
#my $host = `hostname`;
#print "host es: $host fin!\n";

# cicla sobre ellos
foreach $in_file (@in_files) {
     print "$in_file.fastq.gz\n";
   # out_file basado en in_file pero necesitamos cambios
                   
        $out_file = $in_file;
        $out_file =~ s/$in_path/$out_path/;
        $out_file =~ s/.fastq.gz$/.sam/;
        
        $log_file = $in_file;
        $log_file =~ s/$in_path/$log_path/;
        $log_file =~ s/.fastq.gz$/.log/;
          
      
        
    
    
            
                    
    
    system ("bowtie2 -p 24 -x $ebwt_file -1 $in_file.1.fastq.gz -2 $in_file.2.fastq.gz -S $out_file");
       
   

}