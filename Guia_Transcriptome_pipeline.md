#Guia para an�lisis de RNAseq (basado en ejemplo de Octopus maya, trabajo de Laura Lopez)

##Descarga del BaseSpace los archivos .fastq de la secuenciacion

###Seguir indicaciones para conectarse a [BaseSpace](https://basemount.basespace.illumina.com/)
```
sudo bash�-c�"$(curl -L https://basemount.basespace.illumina.com/install)"

basemount BaseSpace/

cd BaseSpace
```
####Esto permite el acceso a todas las corridas del MiSeq 
####Para descargarlas deben estar guardadas como proyectos
####Entrar por la carpeta Projects y buscar el proyecto actual. Luego ubicarse en la carpeta Samples/ y desde ahi ejecutar:

#####para forward
`find . -type f -name "*R1_001.fastq.gz" | xargs cp -t /home/marcos/Documents/XIXIMI/run_07_11_2016/forward_reads/`

#####para reverse
`find . -type f -name "*R2_001.fastq.gz" | xargs cp -t /home/marcos/Documents/XIXIMI/run_07_11_2016/reverse_reads/`

###Una vez que se terminaron de copiar desmontar el acceso a BaseSpace:

`basemount --unmount BaseSpace/`

##Comprimir y copiar los archivos a OMICA

###COMPRIMIR

`tar -cvf fastq07112016.tar *fastq.gz`

####Para subirlo a OMICA:

`scp fastq07112016.tar  paoline@omica:/home/paoline/run_07_11_2016/`

####Descomprimir archivos:

`find . -name "*.gz" -exec gunzip {} \;`

##Para esto se ejecutar�n los siguientes comandos en OMICA:
###para limpiar las secuencias de baja calidad se utiliza [Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic) y el archivo [1_trimmomatic.sh](https://bitbucket.org/quetjaune/rnaseq_transcriptome_pipeline/src/a3fbba2a9758248d6ab50ed08dcc1d4942f6fe49/1_trimmomatic.sh?at=master&fileviewer=file-view-default)

`sbatch 1_trimmomatic.sh`

###Luego, las secuencias limpias se juntan todas las que corresponden a R1 por un lado, y R2 por el otro, en unicos archivos multifasta y luego se realiza el Ensamble de novo del transcriptoma mediante [Trinity](https://github.com/trinityrnaseq/trinityrnaseq/wiki) en [OMICA](http://omica.cicese.mx/docs/submitjobs.html#tareas-interactivas) junto con el archivo [2_trinity.sh](https://bitbucket.org/quetjaune/rnaseq_transcriptome_pipeline/src/a3fbba2a9758248d6ab50ed08dcc1d4942f6fe49/2_trinity.sh?at=master&fileviewer=file-view-default)

`sbatch 2_trinity.sh`

###Una vez obtenido el ensable de novo, se usa este como referencia para mapear las lecturas obtenidas en cada muestra por separado. El mapeo se realiza con [Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml) y usando los archivos [3_bowtie2.pl](https://bitbucket.org/quetjaune/rnaseq_transcriptome_pipeline/src/14e0bc79c0ef8bb7615c6ee7e8083f2a58240d12/3_bowtie2.pl?at=master&fileviewer=file-view-default) y [3_bowtie2.slrm](https://bitbucket.org/quetjaune/rnaseq_transcriptome_pipeline/src/14e0bc79c0ef8bb7615c6ee7e8083f2a58240d12/3_bowtie2.slrm?at=master&fileviewer=file-view-default)
`sbatch 3_bowtie2.slrm`

###
