#!/bin/env perl

$in_regexp = "[S]*"; #para indicar con que letra comienzan los archivos a procesar
$in_path   = "/home/marcos/Documents/Transcriptome/mapeo_bowtie2_long_isof/bam_converted/";
$out_path  = "/home/marcos/Documents/Transcriptome/mapeo_bowtie2_long_isof/sort_bef_merge/";
$inExt   = "bam";

#crear directorio
mkdir $out_path;

my @files = <$in_path/*.$inExt>;

foreach my $in_file (@files) {

   my $name = $in_file;
      $name =~ s/.+\///g;

   print "$name\n";
	my $out_file = "$out_path/$name";           
    
    system ("/LUSTRE/bioinformatica_data/genomica_funcional/bin/samtools-1.3.1/samtools sort $in_file $out_file.sorted");
       }
