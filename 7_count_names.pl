    # Receive a line from SAM, decide what to count, print totals at the end

use warnings;
use strict;

my %counts;
while (my $line = <>) {
   chomp $line;
   # decide what to count
   my (@data) = (split /\t/, $line);
   my $name = $data[2];
   my $col12 = $data[12];

   # ignore reads with no match
   next if $name eq '*';

   # skip if line contains XS
   next if $col12 =~ /^XS/;

   $counts{$name}++;
}

foreach my $name (sort keys %counts) {
   print "$name\t$counts{$name}\n";
}
