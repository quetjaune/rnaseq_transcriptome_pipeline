# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
```
This respository aims to work as a guide for the RNAseq analysis from Samples obtained of wood of Malbec grapevine in INTA field. First the plants were chosen acording to be sure that been working with the same genotypes and identical cultural managements during its 23 years since planting.
three plants showing clear symptoms of "hoja de malvon" disease were chosen and other three that did not show any disease-related symtom. The plants were unrooted and imediately were carried to the laboratory. Trunk and arms were cuted with a saw while the sawdust was collected on a plastic bag 
and imediately was stored at -80 until RNA extraction. RNA extraction was done using a protocol modified from Chang et al., 2008. The obtained RNA was cleaned through Zymo Clean & Concentrator kit according to manufacturer recomendations. The total RNA was sequenced in Illumina Hiseq4000 in Macrogen, South Korea
using libraries prepared with Truseq Plant Ribodepletion.
```
* Version 0.1


### How do I get set up? 

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Pipeline summary

*Check the integrity of downloaded files* That must be the same results from the reported in Macrogen (https://dna2.macrogen.com/upload/lims/NGS_DATA/1808//180806G-035_1804KHF-0143.pdf)
`for f in ../../../raw_data/*fastq.gz; do md5sum $f; done`
the pipeline here presentes was run on Linux Mint 18.1 Cinammon 64 bit in a computer with Intel Core i7-7500 U 2.7GHz x 2 y 15.6 GB of RAM
##Quality Check
(ref: https://blog.horizondiscovery.com/diagnostics/the-5-ngs-qc-metrics-you-should-know)

###Through FastQC
First check java version (required 1.6-1.8)
java -version
openjdk version "1.8.0_171"
OpenJDK Runtime Environment (build 1.8.0_171-8u171-b11-0ubuntu0.16.04.1-b11)
OpenJDK 64-Bit Server VM (build 25.171-b11, mixed mode)

Version reciente 0.11.7 zipeada:
Download FASTQC (linux version) from https://www.bioinformatics.babraham.ac.uk/projects/download.html
```
Then uncompress version 0.11.7 send to /usr/src/
sudo unzip fastqc_v0.11.7.zip -d /usr/src/
cambiar permisos en fastqc
sudo chmod 755 fastqc
Crear directorio para archivos de salida
mkdir fastqc_out
Correr an�lisis con fastqc
for f in ../../../raw_data/*fastq.gz; do /usr/src/FastQC/fastqc $f --extract --outdir=/home/quetjaune/Documents/RNAseq_results_06082018/raw_data/1_quality_check/fastqc_out/; done
```
###Through trimmomatic 
Download version 0.38 desde http://www.usadellab.org/cms/?page=trimmomatic
Decomprimir y enviar a /usr/src/
Correr el an�lisis
```
for f in $(ls *fastq.gz | sed 's/_[1-2].fastq.gz//' | sort -u); do java -jar /usr/src/Trimmomatic-0.38/trimmomatic-0.38.jar PE -threads 8 -basein ${f}_1.fastq.gz -baseout ./1_quality_check/trimmomatic_out/${f}.fastq.gz ILLUMINACLIP:/usr/src/Trimmomatic-0.38/adapters/TruSeq3-PE-2.fa:2:30:10 SLIDINGWINDOW:4:15 LEADING:5 TRAILING:5 MINLEN:25; done
```

##rRNA identification in all trimmed reads

###Through MATAM
Ref: https://github.com/bonsai-team/matam

Instalar MATAM via conda (miniconda_python3.5):
```conda install --channel "bonsai-team" matam```

Construct reference index from SILVA_128_SSURef_NR95 as default DB:
```index_default_ssu_rrna_db.py```

Correr ejemplo con un archivo de 1000secuencias elegidas al azar del archivo c22_1P.fastq 
generar el subsample:
```seqtk sample -s100 c22_1P.fastq 1000 > sub1000_c22_1P.fq```
Corrida en MATAM:
```matam_assembly.py -i /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/1_quality_check/trimmomatic_out/sub1000_c22_1P.fq -d /home/quetjaune/matam/db/SILVA_128_SSURef_NR95 -o /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/3_rRNA_identification_Infernal/matam_all_out/ -v --perform_taxonomic_assignment --cpu 3```
Revisar ejemplo:
```grep -v "Chloroplast" sub1000_c22_1P.sortmerna_vs_SILVA_128_SSURef_NR95_b10_m10.*taxo.tab | grep -v "Mitochond" | cut -f4 | sort -u | more```
###Mapping on Grapevine genome (version 12x.v2)
Download Pinot noir updated 2018 genome assembly and annotation from [here](https://urgi.versailles.inra.fr/Species/Vitis/Annotations)
#### DISCARDED: Mapping through [bowtie2(v2.3.4.2)](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
```
**Preparing references
unzip VV_12X_embl_102_Scaffolds.fsa.zip
**Index construction
bowtie2-build VV_12X_embl_102_Scaffolds.fsa Vvin_scaf
**Haciendo el mapeo:
mkdir bowtie2_out
for f in $(ls *[1-2]P.fastq.gz | sed 's/_[1-2]P.fastq.gz//' | sort -u); 
do bowtie2 --threads 4 -x /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/2_grapevine_mapping/Vvinifera_ref/Vvin_scaf -1 ${f}_1P.fastq.gz -2 ${f}_2P.fastq.gz -S /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/2_grapevine_mapping/bowtie2_out/${f}.sam;
done
```
Get unmapped reads from sam files
```
for f in $(ls *sam); do samtools view -bS -f 12 ${f} > ${f}.unmapped.bam; done
for f in $(ls *bam); do samtools fastq ${f} > ${f}.unmapped.fastq; done
```

MATAM en lecturas que no mapean en vid
Instalando y corriendo en HPC (toko)
```
conda install matam
conda install -c bioconda rdptools
conda install -c bonsai-team gcc-5 
```
Uso el archivo **matam_sbatch.slrm** para correr un ejemplo con 100000 lecturas de la muestra C22
```
seqtk sample -s1000 c22.unmapped.fastq > sub100000_c22.unmap.fq
sbatch matam_sbatch
```

TRY other strategy: Remove all reads that map on plastid and mitochondrial genome of all the organisms available at Refseq (https://www.ncbi.nlm.nih.gov/genome/organelle/)
Downloads DNA and RNA db from plastids (ftp://ftp.ncbi.nlm.nih.gov/refseq/release/plastid/) and from mitochondrion (ftp://ftp.ncbi.nlm.nih.gov/refseq/release/mitochondrion/)
```
1) concatenate all
cat plastid.2.*fna mitochondrion.*fna > all_organelle_ADN_ARN.fasta
2) Construct Bowtie2 index
bowtie2-build all_organelle_ADN_ARN.fasta all_org
3) Map reads from grpvne_genome unmapped files on all available organelle genomes
for f in $(ls *[0-9:0-9].unmapped.fastq | sed 's/.unmapped.fastq//' | sort -u); 
do bowtie2 --threads 4 -x /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/4_organelle_reads_remotion/bowtie2_out_all_org/all_org -U ${f}.unmapped.fastq -S /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/4_Brassica_napus_organ_remotion/bowtie2_out/${f}_all_org_filt.sam;
done
4) Get unmapped reads from sam files
for f in $(ls *sam | sed 's/_all_org_filt.sam//'); do samtools view -bS -f 4 ${f}_all_org_filt.sam | samtools fastq - > ${f}_all_org_unmap.fq; done
5) subsample c22 to 100000 reads and run MATAM
seqtk sample -s100 c22_all_org_unmap.fq 100000 > sub100000_c22_all_org_unmap.fq
matam_assembly.py -i /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/4_organlle_reads_remotion/bowtie2_out_all_org/sub100000_c22_all_org_unmap.fq -d /home/quetjaune/matam/db/SILVA_128_SSURef_NR95 -o /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/3_rRNA_identification_Infernal/matam_all_org_filt_out/ -v --perform_taxonomic_assignment --cpu 3 --max_memory 8000`
```
RESULT: the SAME as before

I would try to remove plant rRNA reads (Despite the librarie were constructed using plant ribozero kit, still remain plant rRNA reads in output file)
Using the most representative sequence obtained as MATAM output (rRNA from A. thaliana) as query in NCBI blast, I select the best 100 hits complete sequences and download it, then regions containing Ns were removed through sed and the resulting rRNA_NCBI.fasta file was used for filter
BOWTIE2
```
1) contruct the index
bowtie2-build rRNA_ncbi.fasta plant_rRNA
2) filter the unmapped reads of c22
bowtie2 --threads 3 -x plant_rRNA --un ./bowtie2_out/plant_rRNA_remotion.fq -U /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/4_organelle_reads_remotion/bowtie2_out_all_org/c22_all_org_unmap.fq 
```
WITH ALL THE SAMPLES
FIRST: FILTER OUT THE ORGANELLE READS
```
for f in $(ls *[0-9:0-9].unmapped.fastq | sed 's/.unmapped.fastq//' ); 
do bowtie2 -p 3 -x /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/4_organelle_reads_remotion/all_org \
-U ${f}.unmapped.fastq | samtools view -bS -f 4 - | samtools fastq - > /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/4_organelle_reads_remotion/bowtie2_out_all_org/${f}_all_org_unmap.fq ; 
done
```
SECOND: FILTER OUT THE PLANT rRNA reads
```
for f in $(ls *all_org_unmap.fq | sed 's/_all_org_unmap.fq//' ); 
do bowtie2 -p 3 -x /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/3_rRNA_identification_Infernal/plant_rRNA_remotion/plant_rRNA \
-U ${f}_all_org_unmap.fq | samtools view -bS -f 4 - | samtools fastq - > /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/3_rRNA_identification_Infernal/plant_rRNA_remotion/bowtie2_out/${f}_plant_rRNA_unmap.fq; 
done
```
3) MATAM ASSEMBLY and TAXONOMIC ASSIGNMENT
```
matam_assembly.py -i plant_rRNA_remotion.fq -d /home/quetjaune/matam/db/SILVA_128_SSURef_NR95 -o /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/3_rRNA_identification_Infernal/matam_norrna_out/ \
-v --cpu 3 --max_memory 8000 --perform_taxonomic_assignment
```

Results interesting in krona (file:///home/quetjaune/Documents/RNAseq_results_06082018/raw_data/3_rRNA_identification_Infernal/matam_norrna_out/workdir/scaffolds.NR.min_500bp.abd.rdp.fltr.krona.html). But only Bacteria taxonomic assignment and none eukaryota, probably because the model was 16S, so I would try with LSU fungal model
WITH ALL THE SAMPLES IN TOKO
```
1) Compress and cp files to toko
tar -zcvf all_grpvne_samples.tar.gz /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/3_rRNA_identification_Infernal/plant_rRNA_remotion/bowtie2_out/
scp all_grpvne_samples.tar.gz mpaolinelli@toko.uncu.edu.ar:/home/mpaolinelli/unmap_org_plant_rRNA/ 
2) Get into TOKO and uncompress 
ssh mpaolinelli@toko.uncu.edu.ar
cd /home/mpaolinelli/unmap_org_plant_rRNA/ & tar -zxvf all_grpvne_samples.tar.gz
2) Run MATAM in TOKO (nodo05) estimated time:36 hs
sbatch matam_multi_sbatch
3) Download *krona.html and *krona.tab to de_toko folder for each sample
scp mpaolinelli@toko.uncu.edu.ar:/home/mpaolinelli/matam_out/c25/workdir/*krona.tab ./c25_scaffolds_500_krona.tab
```
RESULTS
C22:file:///home/quetjaune/Documents/RNAseq_results_06082018/raw_data/3_rRNA_identification_Infernal/matam_norrna_out/de_toko/c22_scaffolds.NR.min_500bp.abd.rdp.fltr.krona.html
C23:file:///home/quetjaune/Documents/RNAseq_results_06082018/raw_data/3_rRNA_identification_Infernal/matam_norrna_out/de_toko/c23_scaffolds_500_krona.html
C25:file:///home/quetjaune/Documents/RNAseq_results_06082018/raw_data/3_rRNA_identification_Infernal/matam_norrna_out/de_toko/c25_scaffolds_500_krona.html

####GIVE UP IT BECAUSE OF INCOMPABILITY IN TAXONOMIC FILES#####
ASSEMBLY using fungal UNITE-ITS model in personal computer
Download DB from https://www.mothur.org/wiki/File:Unite_ITS_02.zip and uncompress
```
wget https://www.mothur.org/wiki/File:Unite_ITS_02.zip
unzip Unite_ITS_02.zip
cd Unite_ITS_02
matam_db_preprocessing.py -i UNITEv6_sh_dynamic.fasta -d ITS_DB --cpu 3 --max_memory 8000
matam_assembly.py -i plant_rRNA_remotion.fq -d /home/quetjaune/matam/db/SILVA_128_SSURef_NR95 -o /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/3_rRNA_identification_Infernal/matam_norrna_out/ \
-v --cpu 3 --max_memory 8000 --perform_taxonomic_assignment --training_model fungallsu
```
############################

USING SILVA LSU AS DATABASE
```
1) preparing db
wget https://www.arb-silva.de/fileadmin/silva_databases/release_132/Exports/SILVA_132_LSURef_tax_silva.fasta.gz
gunzip SILVA_132_LSURef_tax_silva.fasta.gz
matam_db_preprocessing.py -i SILVA_132_LSURef_tax_silva.fasta --cpu 3 --max_memory 8000 (take more than 1 hour to finish)
2) Run matam assembly
matam_assembly.py -i ../../plant_rRNA_remotion/bowtie2_out/c22_plant_rRNA_unmap.fq -d ../SILVA_132_LSURef_tax_silva_NR95 -o ./ -v --cpu 3 --max_memory 8000 --perform_taxonomic_assignment --training_model fungallsu
```
RESULT: Excelent!!! Check in file:///home/quetjaune/Documents/RNAseq_results_06082018/raw_data/3_rRNA_identification_Infernal/matam_SILVA_LSU_DB/matam_out/workdir/scaffolds.NR.min_500bp.abd.rdp.fltr.krona.html

NOW, DO ALL THE SAMPLES IN TOKO
```
1) TAR the SILVA_LSU DB and upload to toko
tar -cvf SILVA_132_LSURef_tax_silva_NR95.tar SILVA_132_LSURef_tax_silva_NR95.c*
scp SILVA_132_LSURef_tax_silva_NR95.tar mpaolinelli@toko.uncu.edu.ar:/home/mpaolinelli/matam/db/SILVA_132_LSURef/
2) then Use sbatch
sbatch matam_multi_LSU_sbatch
```
####COMPARE BACTERIAL BIODIVERSITY AMONG SAMBLES####
1)First download the scaffolds assembly and rdp classification from TOKO
```
for d in "c22" "c23" "c25" "m11" "m16" "m26"; 
do scp mpaolinelli@toko.uncu.edu.ar:/home/mpaolinelli/matam_out/${d}/final_assembly.fa ./${d}_final_assembly.fa; 
done
for d in "c22" "c23" "c25" "m11" "m16" "m26"; do scp mpaolinelli@toko.uncu.edu.ar:/home/mpaolinelli/matam_out/${d}/rdp.tab ./${d}_rdp.tab; done
2) construir el archivo para indicar paths de muestras para archivos fasta y rdp, terminar de editar detalles con nano
for d in "c22" "c23" "c25" "m11" "m16" "m26"; do ls $PWD/$d* >> samples_file; done
3) Correr comparacion con matam
matam_compare_samples.py -s samples_file -t abundance_table.txt -c taxonomy_table.txt
```
####COMPARE FUNGAL BIODIVERSITY AMONG SAMBLES####

####Scaffolds from bacterial contigs obtained for all samples####
```
1) Download contigs fasta file from TOKO
for d in "c22" "c23" "c25" "m11" "m16" "m26"; do scp mpaolinelli@toko.uncu.edu.ar:/home/mpaolinelli/matam_out/${d}/workdir/contigs.fasta ./${d}_contigs_bact.fasta; done
1) prepare the unique ID to filter the common scaffolds in all the samples
grep -v "unclassified" *tab | sort -u -k17,17 | cut -f1 > unique_genus_bact_ID
2) concatenate the final_assembly.fa file in one superfasta file and remove the counts
cat *final_assembly.fa > super_scaffolds.fa
sed -i 's/count=[0-9]*.[0-9]*//g' super_scaffolds_all_samples.fa
Identificar duplicados y quitarlos mediante nano
grep ">" super_scaffolds_all_samples.fa | sed 's/>//g' | sort | uniq -d > dup_in_fasta
2) Use ID to filter the scaffolds fasta file
faidx super_scaffolds_all_samples.fa 1768 1640 2247 2012 1916 1101 2562 2282 1262 623 1901 2285 1131_1 2165 3649 3256 2203 > super_scaffolds_all_samples_uniq.fa
3) Download rRNA mapped reads in fq file from TOKO
for d in "c22" "c23" "c25" "m11" "m16" "m26"; do scp mpaolinelli@toko.uncu.edu.ar:/home/mpaolinelli/matam_out/${d}/workdir/*.fq ./${d}_rrna_map_bact.fq; done
4) Map the fastq files in common bacterial scaffolds to construct a counts matrix
bowtie2-build super_scaffolds_all_samples_uniq.fa sup_scaff_bact
for f in $(ls *bact.fq | sed 's/_rrna_map_bact.fq//' ); do bowtie2 -p 3 -x sup_scaff_bact -U ${f}_rrna_map_bact.fq -S ${f}_4_count.sam; done 
5) Conversion to bam and sort
for f in $(ls *_4_count.sam | sed 's/_4_count.sam//' ); do samtools view -Sb ${f}_4_count.sam | samtools sort - > ${f}_sorted.bam; done
6) Contar las lecturas mapeadas
for f in $(ls *_sorted.bam | sed 's/_sorted.bam//'); do samtools view ${f}_sorted.bam | perl /home/quetjaune/Documents/Transcriptome_ex_Pulpo/scripts_4_Transcriptome/count_names.pl  > ${f}.stats; done
7) then construct table of counts matrix with script in R 8_build_count_table.R
```
RESULT: `more table_counts.txt`
	c22.stats	c23.stats	c25.stats	m11.stats	m16.stats	m26.stats
1101	17	17	12	15	9	7
1131_1	274	363	241	278	153	86
1262	243	317	145	211	147	44
1640	4	15	0	9	3	2
1768	36	78	36	33	24	1
1901	30	53	18	37	18	2
1916	56	150	67	89	42	10
2012	701	1246	749	578	641	440
2165	74	237	89	98	81	42
2203	61	174	97	35	79	20
2247	10	28	11	36	22	14
2282	100	116	84	93	89	42
2285	433	464	296	329	201	111
2562	18	118	273	2	0	0
3256	27	104	22	28	15	11
3649	245	438	629	258	288	227
623	82	213	118	104	59	156
HALOCOCCUS ARCHAEAE EXPLAIN THE DIFFERENCES BETWEEN SYMPTOMATIC (M) AND ASYMPTOMATIC PLANTS (C)
```
8) I would try to assemble the concatenated rrna mapping reads for all the samples through MATAM
cat *bact.fq > super_rrna_map_bact.fq
matam_assembly.py -i super_rrna_map_bact.fq -d /home/quetjaune/matam/db/SILVA_128_SSURef_NR95 -o ./ -v --cpu 3 --max_memory 8000 --perform_taxonomic_assignment 
#suspendido por que se generaba archivo de 160 Gb
```
#BACTERIA
```
/home/quetjaune/idba/bin/idba_tran -r super_rrna_map_bact.fq -o idba_bact_out
bowtie2-build contig.fa sup_idba_assem
for f in $(ls ../[cm]*bact.fq | cut -f2 -d"/" | sed 's/_rrna_map_bact.fq//'); do bowtie2 -p 3 -x sup_idba_assem -U ../${f}_rrna_map_bact.fq -S ${f}_4_count.sam; done
5) Conversion to bam and sort
for f in $(ls *_4_count.sam | sed 's/_4_count.sam//' ); do samtools view -Sb ${f}_4_count.sam | samtools sort - > ${f}_sorted.bam; done
6) Contar las lecturas mapeadas
for f in $(ls *_sorted.bam | sed 's/_sorted.bam//'); 
do samtools view ${f}_sorted.bam | perl /home/quetjaune/Documents/Transcriptome_ex_Pulpo/scripts_4_Transcriptome/count_names.pl  > ${f}.stats; 
done
7) then construct table of counts matrix with script in R 8_build_count_table.R and evaluate statistical differences through 9_differential_diversity_analysis.R
```

#FUNGI
```
9) Check assembly with IDBA-tran and then map ando count with bowtie2
/home/quetjaune/idba/bin/idba_tran -r super_lsu_map_fungal.fq -o idba_fungal_out`
bowtie2-build contig.fa sup_idba_assem
for f in $(ls ../[cm]*bact.fq | cut -f2 -d"/" | sed 's/_rrna_map_bact.fq//'); do bowtie2 -p 3 -x sup_idba_assem -U ../${f}_rrna_map_bact.fq -S ${f}_4_count.sam; done
5) Conversion to bam and sort
for f in $(ls *_4_count.sam | sed 's/_4_count.sam//' ); do samtools view -Sb ${f}_4_count.sam | samtools sort - > ${f}_sorted.bam; done
6) Contar las lecturas mapeadas
for f in $(ls *_sorted.bam | sed 's/_sorted.bam//'); do samtools view ${f}_sorted.bam | perl /home/quetjaune/Documents/Transcriptome_ex_Pulpo/scripts_4_Transcriptome/count_names.pl  > ${f}.stats; done
7) then construct table of counts matrix with script in R 8_build_count_table.R and evaluate statistical differences through 9_differential_diversity_analysis.R
```
head -20 malvon_vs_control.txt
logFC	logCPM	LR	PValue	FDR
contig-60_1942	-5.88746239753116	9.75580451450939	20.6317173046774	5.56662194645808e-06	0.00890659511433293
contig-60_29	-4.27100744799009	10.5795810156068	14.0255089125154	0.000180347359141519	0.106316729801168
contig-60_392	-3.73744668789276	10.8829982677797	13.8372582260603	0.000199343868377189	0.106316729801168
contig-60_743	7.46376967332477	8.25264929386121	12.0863371536163	0.000507926607555817	0.172035385658726
contig-60_2922	5.01660991288754	8.9598781739959	11.9804687756676	0.000537610580183519	0.172035385658726
contig-60_644	4.79391992909305	11.1824806841205	10.5079631547062	0.00118861201097432	0.316963202926486
contig-60_465	4.52786719858937	7.41589155243344	10.134304974854	0.00145535108407659	0.325019808263281
contig-60_83	4.92502816288191	7.58018241942043	9.93109415690597	0.00162509904131641	0.325019808263281
contig-60_2871	-4.22459640800782	7.59372269807919	9.58292912122973	0.00196394830167409	0.34914636474206
contig-60_14	3.39831305729497	8.57882917002225	9.19349455992428	0.00242876758654578	0.373010371771989
contig-60_1711	5.13483109743126	6.66237262113487	8.88933880231029	0.00286840588352484	0.373010371771989
contig-60_2455	-2.37929999503823	13.0512026456486	8.87766777737262	0.00288680087022157	0.373010371771989
contig-60_2575	-5.19758260703211	6.75488345275168	8.78888804047979	0.00303070927064741	0.373010371771989
contig-60_2795	4.06359314749047	11.4174193930099	8.11856310555209	0.00438142722165353	0.453264917634099
contig-60_329	-5.19376397739883	6.75002036360896	8.05267854041372	0.004543642668187	0.453264917634099
contig-60_838	4.59784729782592	9.01248850655922	7.99762122876806	0.00468388434805887	0.453264917634099
contig-60_1662	-3.93562150558214	7.28676793142396	7.94729154355317	0.00481593974986231	0.453264917634099
contig-60_466	-4.32030246221644	7.49491912922283	7.82129327178898	0.0051634234603136	0.458970974250097
contig-60_1997	-4.66590998924262	6.48714578875893	7.68115593206034	0.00558004136948028	0.465068608046821

USING FUNGI RDP DATABASE
```
mkdir matam_RDP_FUNGI_DB_out $ cd matam_RDP_FUNGI_DB
wget https://rdp.cme.msu.edu/download/current_Fungi_unaligned.fa.gz
gunzip current_Fungi_unaligned.fa.gz
```
###NOT EXPLORED AT ALL####
```
1) Install infernal.1.1.2 through conda
conda install -c bioconda infernal=1.1.2
 2) Download RM database
 wget ftp://ftp.ebi.ac.uk/pub/databases/Rfam/12.1/Rfam.cm.gz
 gunzip Rfam.cm.gz
 cmpress Rfam.cm
 cmscan --rfam --cut_ga --nohmmonly --tblout mrum-genome.tblout --fmt 2 Rfam.cm /home/quetjaune/Documents/RNAseq_results_0682018/raw_data/4_organelle_reads_remotion/bowtie2_out_all_org/sub100000_c22_all_org_unmap.fq
```
 #####NOT EXPLORED AT ALL#####

####23/10/2018####
###Microorganisms identification through IMSA-A##
Ref: https://github.com/JeremyCoxBMI/IMSA-A
Install everything according to IMSA+A_Detailed_Directions.docx
Install oases0.2.9 from https://github.com/dzerbino/oases
```
1) run transcriptome assembly from grapevine/organelles/plant_rRNA unmapped reads, run assembly independently for each sample, but I will try first only with c22
I could not use oases, so alternatively, I make the assembly using default parameters in Trinity using all the samples reads together, I will use the assembled file to use in blast on IMSA-Fungi Database
Download the inchworm file from OMICA###
scp paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/Trinity_all_assembly_out/inchworm.K25.L25.DS.fa ./
####Run Blast
DATABASE_FILE=/home/quetjaune/Documents/RNAseq_results_06082018/raw_data/5_microog_divers_explor_IMSA/imsa-a_all_out/microbiome/NCBI.fungiDBselect.genomeonly
blastn -max_target_seqs 200 -num_threads 3 -evalue 1e-15 -outfmt 6 -db $DATABASE_FILE -query inchworm.K25.L25.DS.fa -out inchworm_snt.bln
```
####RESULT: 1851738 hits##
#### Now extract the interesting info from that blastn output file. First the systemSettings.py file must be modified to include the appropriate PATHS for fungiDB and gi_taxid files
```
python2.7 /home/quetjaune/IMSA-A/imsa_v2/IMSA-A-master/postprocesscount4.py -b inchworm_snt.bln
```
RESULT: Fungal, bacterial and viral families, genus and species, or firstTAxon were retained; but seems important to update database with the genomes of MO expected to be found in our samples

####THE DATABASE used as references both for MATAM (rRNA) or IMSA(genome) strategies to characterize the microbiome in Grpvne MUST BE UPDATED WITH THE MICROORGANISMS EXPECTED TO BE FOUND TO HAVE A CONFIDENCE DIAGNOSTIC##
####Lets gonna try with SAMSA2 (https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-018-2189-z) pipeline, that have the option for use a customizable database
```
####Install SAMSA2 (https://github.com/transcript/samsa2), clone from github 
git clone https://github.com/kaedenn/samsa2.git
####INstall the required packages
bash setup_and_test/package_installation.bash
####Download from REFseq FTP (ftp://ftp.ncbi.nlm.nih.gov/refseq/release/complete/) the non-redundant protein fasta file: complete.nonredundant_protein.99.protein.faa.gz (25/10/18) 
and also download the last version from fungi, bacteria, archaeal and viral:
wget ftp://ftp.ncbi.nlm.nih.gov/refseq/release/fungi/fungi.9.protein.faa.gz
wget ftp://ftp.ncbi.nlm.nih.gov/refseq/release/bacteria/bacteria.nonredundant_protein.99.protein.faa.gz
wget ftp://ftp.ncbi.nlm.nih.gov/refseq/release/archaea/archaea.nonredundant_protein.7.protein.faa.gz
wget ftp://ftp.ncbi.nlm.nih.gov/refseq/release/viral/viral.3.protein.faa.gz
####concatenate files to have a complete DB
cat fungi.9.protein.faa.gz bacteria.nonredundant_protein.99.protein.faa.gz archaea.nonredundant_protein.7.protein.faa.gz viral.3.protein.faa.gz > complete_protein.faa.gz
gunzip complete_protein.faa.gz
####prepare DB
/home/quetjaune/samsa2/programs/diamond makedb --in complete_protein.faa --db complete_protein.dmnd
####modify the example DIAMOND_example_script.bash with nano and run using sample c22
bash /home/quetjaune/samsa2/bash_scripts/DIAMOND_example_script.bash
####Sort/Group results based on Organisms (-O parameter) or function (-F parameter)
python2.7 /home/quetjaune/samsa2/python_scripts/DIAMOND_analysis_counter.py -I c22_unmap_BLAST_m8_results.m8  -D complete_proteins.faa -O
```
The results show that there is low representation of fungal proteins in DB, so we need to
#### find a representative DB that contain homogeneous representation for each group of MO, but anyway, just to finish the program evaluation, 
let's continue with R analysis through SAMSA2 wrapper
modify the DIAMOND_example_script.bash file to process the remaining samples c23, c25, m11, m16 and m26
```
####then process each command on one command through a loop both for organism and for function
for f in [cm]*m8; do python2.7 /home/quetjaune/samsa2/python_scripts/DIAMOND_analysis_counter.py -I $f  -D complete_protein.faa -O; done
for f in [cm]*m8; do python2.7 /home/quetjaune/samsa2/python_scripts/DIAMOND_analysis_counter.py -I $f  -D complete_protein.faa -F; done
####then I need to rename files as control_ instead of c and experimental_ instead of m
for f in $(ls c*_organism.tsv); do mv $f control_$f; done
for f in $(ls m*_organism.tsv); do mv $f experimental_$f; done
for f in $(ls c*_function.tsv); do mv $f control_$f; done
for f in $(ls m*_function.tsv); do mv $f experimental_$f; done
####NOW let's try with R scripts, use the diversity_stats_and_graphs.R script in RStudio to construct the table with counts for each organism calculate Shannon and Simpson Diversity values for symptomatic (m) and asymptomatic (c) plants and PCA, Stacked bars and diversity index graphs
```
####RESULTS: No differences among symptomatic and asymptomatic plants in terms of diversity

####Virus identification###
```
###Assembly with IDBA-TRANS of all the unmapped(grpvne+organelles+plantrrna) reads####
/home/quetjaune/idba/bin/idba_tran -r <(cat /home/quetjaune/Documents/RNAseq_results_06082018/raw_data/3_rRNA_identification_Infernal/plant_rRNA_remotion/bowtie2_out/[cm]*) -o idba_all_unmap_out # Imposible in my lap
####try with TRINITY in OMICA HPC
sbatch Trinity_4_microbes.slrm
####Main parameters
/LUSTRE/bioinformatica_data/genomica_funcional/bin/trinityrnaseq/Trinity --seqType fq --max_memory 60G --single c22_plant_rRNA_unmap.fq, c25_plant_rRNA_unmap.fq, m16_plant_rRNA_unmap.fq, \
c23_plant_rRNA_unmap.fq, m11_plant_rRNA_unmap.fq, m26_plant_rRNA_unmap.fq --run_as_paired --no_bowtie --CPU 4 --output Trinity_unmpa_grpvne_assembly_out
```
####first I make an assembly test with only C22 sample, Download the output
```
rsync -v -e ssh paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/Trinity/Trinity_unmpa_grpvne_assembly_out/Trinity.fasta ./c22_only_trin_assem.fasta
```
####Download the assembly obtained with all the samples file all_samples_trin_assem.fasta (674519 sequences/contigs, after clustering on 90% identity remains 622912 seqs)

##CHECK VIRUS IN C22 unmaped reads assembly##
####Download virus database genomic [sequences](ftp://ftp.ncbi.nlm.nih.gov/refseq/release/viral/viral.3.1.genomic.fna.gz) and [protein](ftp://ftp.ncbi.nlm.nih.gov/refseq/release/viral/viral.3.protein.faa.gz)
```
####RUN BLASTN on Virus nucleotide DB
makeblastdb -in viral.3.1.genomic.fna -dbtype nucl
blastn -query c22_only_trin_assem.fasta -db viral.3.1.genomic.fna -outfmt 6 -out c22_blastn_viral3.tab
####RUN BLASTX on Virus protein DB
makeblastdb -in viral.3.protein.faa -dbtype prot`
blastx -query c22_only_trin_assem.fasta -db viral.3.protein.faa -outfmt 6 -out c22_blastx_viral3.tab
```
###IN RVDB database download the last release of database [RVDB](https://rvdb-prot.pasteur.fr/files/U-RVDBv13.0-prot.fasta.bz2)
```
####uncompress
bzip2 -dv U-RVDBv13.0-prot.fasta.bz2
makeblastdb -in U-RVDBv13.0-prot.fasta -dbtype prot
blastx -query ../c22_only_trin_assem.fasta -num_threads 3 -db U-RVDBv13.0-prot.fasta -outfmt 6 -evalue 1e-50 -max_target_seqs 1 -out c22_blastx_RVDB_evlue-50.tab
####select only the sequences of proteins assigned to grapevine virus, select only sequences that contain Grapevine term in header of fasta and contruct the blastdb index with the resulting file (have around 8500 sequences)
####This was done using blastx, then to obtain the complete Info from unique assigned virus, I use:
cut -f3 -d "|" all_samp_derep_grpvneRVDB_e-50.tab | sort -u > Id_virus_all_samp_RVDB
grep -f Id_virus_all_samp_RVDB U-RVDBv13.0-prot.fasta | sed 's/>//' > hit_blastx_unique_RVDBGrpvne_all_samp.txt
```

###Check on Tombuviridae family, Download fasta file from [NCBI](https://www.ncbi.nlm.nih.gov/protein/?term=txid39738[Organism:exp])
```
####Remove white spaces
sed -i '/^$/d' tombusviridae_ncbi_db_131118.fasta
####Construct db and blastx the contigs obtained from all the Grpvne samples
makeblastdb -in tombusviridae_ncbi_db_131118.fasta -dbtype prot
blastx -query ../all_samp_trin_assem_derep.fasta -num_threads 3 -db tombusviridae_ncbi_db_131118.fasta -outfmt 6 -evalue 1e-50 -max_target_seqs 1 -out allsamp_blastx_tombusviridae_evlue-50.tab
```
###RESULT: 13 hits; extract fasta
```
seqkit grep -f <(cut -f1 allsamp_blastx_tombusviridae_evlue-50.tab) ../all_samples_trin_assem.fasta > all_samp_contigs_tomusviridae_13hits.fasta
```
##UPDATE/ENRICH SILVA DATABASE INCLUDING "EXPECTED TO BE FOUND" Organisms in our samples (file with names: Sequences_to_enrich_SILVA_LSU_SSU_DB.ods)
```
First download all the sequences for each organism from ENA-EMBL database
####the search was done one organism at a time, and all the non-coding sequences (mainly the releases, not the update) both for LSU or SSU regions were downloaded as independent fasta files that were finally concatenated in one fasta file
cat ../../Downloads/ena*.fasta >> grpvne_pathogens_ena_dwnload.fasta
####DEreplicate the fasta file, but first extremely long sequences from Agrobacterium were removed
/home/quetjaune/cdhit/cd-hit-est -M 8000 -i grpvne_pathogens_ena_dwnload_edit3.fasta -o grpvne_pathogens_ena_dwnload_derep.fasta
####then some manual (using nano) editing to remove N's containing sequences and also those flagged as UNVERIFIED OR UNCULTURED. Also larger sequences were prioritized
####Also is required to include taxonomy lineages, so is required to convert accesion number to tax_id and then to lineages, for NCBI model with the R package taxize
grep ">" grpvne_pathogens_ena_dwnload_derep.fasta | cut -f2-3 -d " " | sort -u > grpvne_path_derep_names
sed 's/Bradyrhizobium japonicum//' grpvne_path_derep_names | sed 's/Azorhizobium caulinodans//;s/Campylocarpon sp.//;s/Fusarium cf.//;s/H.schweinitzii rRNA//;s/Sinorhizobium fredii//;s/Trichoderma cf.//' \
| sed '/^$/d' > grpvne_path_derep_names_edit
####NOW using tax_name function in taxize package retrieve from ncbi (script: Get_NCBI_taxonomic_UID_GBaccnumber.R) the taxonomic lineage for each specie name from 18S sequence. The otuput is in file name2tax_taxid_out.tsv
head name2tax_taxid_out.tsv
```
###RESULT:
db	query	superkingdom	kingdom	subkingdom	phylum	subphylum	class	order	family	genus	specie
ncbi	Acremonium sp.	Eukaryota	Fungi	Dikarya	Ascomycota	Pezizomycotina	Sordariomycetes	Hypocreales	NA	Acremonium	NA
ncbi	Alternaria sp.	Eukaryota	Fungi	Dikarya	Ascomycota	Pezizomycotina	Dothideomycetes	Pleosporales	Pleosporaceae	Alternaria	NA
ncbi	Arthrinium sacchari	Eukaryota	Fungi	Dikarya	Ascomycota	Pezizomycotina	Sordariomycetes	Xylariales	Apiosporaceae	Arthrinium	NA
ncbi	Aureobasidium pullulans	Eukaryota	Fungi	Dikarya	Ascomycota	Pezizomycotina	Dothideomycetes	Dothideales	Saccotheciaceae	Aureobasidium	NA
ncbi	Botryosphaeria corticola	Eukaryota	Fungi	Dikarya	Ascomycota	Pezizomycotina	Dothideomycetes	Botryosphaeriales	Botryosphaeriaceae	Diplodia	NA
ncbi	Botryosphaeria dothidea	Eukaryota	Fungi	Dikarya	Ascomycota	Pezizomycotina	Dothideomycetes	Botryosphaeriales	Botryosphaeriaceae	Botryosphaeria	NA
ncbi	Botryosphaeria rhodina	Eukaryota	Fungi	Dikarya	Ascomycota	Pezizomycotina	Dothideomycetes	Botryosphaeriales	Botryosphaeriaceae	Lasiodiplodia	NA
ncbi	Botryosphaeria stevensii	Eukaryota	Fungi	Dikarya	Ascomycota	Pezizomycotina	Dothideomycetes	Botryosphaeriales	Botryosphaeriaceae	Diplodia	NA
ncbi	Cadophora luteo-olivacea	Eukaryota	Fungi	Dikarya	Ascomycota	Pezizomycotina	Leotiomycetes	Helotiales	NA	Cadophora	NA
```
####change the order of columns
paste <(cut -f3-11 name2tax_taxid_out.tsv) <(cut -f2 name2tax_taxid_out.tsv) > name2tax_taxid_out_reord.tsv
####then replace any "NA" for "Unknown_[rank]" through nano and then change "tabs" to ";"
grep Eukaryota name2tax_taxid_out_reord.tsv | sed 's/\t/;/g' > name2tax_taxid_out_reord.csv
#### On the other side, I have to construct the file that help to guide the replacement for fusion_files.sh
grep ">" grpvne_pathog_lengths | cut -f1-3 -d" " | cut -f3-4 -d "|" | sort -u | sed 's/ /\t/' > accnumber_to_spec_name`
####And then contruct the lineage guide file using fusion_files.sh script, and a short edition to the output
bash fusion_files.sh > accnumber_to_lineage
sed 's/ Eukaryota;Fungi/\t/g' accnumber_to_lineage | cut -f1-2 > accnumber_to_lineage_final
####Finally use the lineage guide file to replace headers in fasta, through nano editing of fasta_head_replace.py script
python fasta_head_replace.py
```
###Right now I have the option to modify completely the headers of SILVA_SSU_132 DB to construct a completely new DB that include all the new sequences (specially for those common pathogens in grapevine and well reported isolates involved in GTD from TrunkDiseaseID.org)
```
###First, extract only the specie/genus name (last rank) from the headers, using this list, retrieve the taxonomic lineage from NCBI using taxize package in R. Replace the older taxonomic lineage with the new one.
grep ">" SILVA_132_SSURef_tax_silva.fasta | sed 's/\..*;/\t/' | sed 's/>//' > accnumber_SILVASSU132_to_specname
###Separate the unidentified species (20435) and delete it from the file and cut only the name containing column
grep unidentified accnumber_SILVASSU132_to_specname > accnumber_SILVASSU132_to_unidentified
sed -i '/unidentified/d' accnumber_SILVASSU132_to_specname
cut -f2 accnumber_SILVASSU132_to_specname > specname_from_SILVA
```
###Now use taxid in R (script: Get_NCBI_taxonomic_UID_GBaccnumber.R), This will require long time and a perfect web connection ##Too much time so, I decided to make it local, for that I download the taxid files from ncbi
```
wget ftp://ftp.ncbi.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz
gunzip -c new_taxdump.tar.gz | tar xf - 
```
###Then I need to get only the uniques names for query and then use the fullnamelineage.dmp file for search on it and retrieve only the first hit, (take aroun 3 hours to complete)
```
sort -u specname_from_SILVA > specname_from_SILVA_uniq
cat specname_from_SILVA_uniq | xargs -I{} grep -m 1 -w {} fullnamelineage.dmp > specname_from_SILVA_ncbi_lineage
```
###Some editing is required with the output to sort it similar to what is expected in SILVA examples DB files
```
paste <(cut -f5 specname_from_SILVA_ncbi_lineage) <(cut -f3 specname_from_SILVA_ncbi_lineage) | cut -f3-12 -d " " | sed 's/; /;/g;s/\t//g' | sort -u > specname_SILVA_ncbi_lineag_reord
```
###Edit fushion_files.sh to modify accnumber_SILVASSU132_to_specname file (limit hust to the first hit with m-1 in grep)
```
sed -i 's/\t/ /' accnumber_SILVASSU132_to_specname
bash fusion_files.sh | sed '/ NA/d' > accnumber_SILVASSU132_to_lineage
```
### Also the fasta header needs dome modifications (remove duplicates, remove sequences of unidentified species and change the accnumber_ID(without the number after first dot)
```
sed 's/\.[0-9]*\.[0-9]*//' SILVA_132_SSURef_tax_silva.fasta > SILVA_132_SSURef_tax_silva_shortID.fasta
seqkit rmdup -n SILVA_132_SSURef_tax_silva_shortID.fasta > SILVA_132_SSURef_tax_silva_shortID_nodup.fasta
seqkit grep -f <(cut -f1 accnumber_SILVASSU132_to_specname) SILVA_132_SSURef_tax_silva_shortID_nodup.fasta > SILVA_132_SSURef_tax_silva_nodup_nounident.fasta
seqkit grep -f <(cut -f1 -d" " accnumber_SILVASSU132_to_lineage) SILVA_132_SSURef_tax_silva_nodup_nounident.fasta > SILVA_132_SSURef_tax_silva_to_replace.fasta
```
###Finally fuse the edited SILVA_fasta file with the header of accnumber_SILVASSU132_to_lineage, editing the python file
```
python fasta_head_replace.py
```
##EVALUATE GRAPEVINE GENE EXPRESSION##
```
####From the previously mapping of reads to grapevine genome (version 12x.v2), work on SAM files to count the mapped reads in each gene
1) Conversion to bam and sort
for f in $(ls *.sam | sed 's/.sam//' ); do samtools view -Sb ${f}.sam | samtools sort - > ${f}_sorted.bam; done
2) Contar las lecturas mapeadas
for f in $(ls *_sorted.bam | sed 's/_sorted.bam//'); do samtools view ${f}_sorted.bam | perl /home/quetjaune/Documents/Transcriptome_ex_Pulpo/scripts_4_Transcriptome/count_names.pl  > ${f}.stats; done
Apparently bowtie2 cannot take information regarding splice junction and therefore cannot be used for mapping on genomes, 
so I need to download and install STAR which was constructed to rule out with this kinfd of data
wget https://github.com/alexdobin/STAR/archive/2.6.1c.tar.gz
tar -xvzf 2.6.1c.tar.gz 
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
Marcos Paolinelli
* Other community or team contact
