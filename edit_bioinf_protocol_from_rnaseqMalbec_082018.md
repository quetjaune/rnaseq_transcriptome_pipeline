#ACTIVITIES
##GRAPEVINE GENE EXPRESSION
###1)Mapping on grapevine genome and count unique mapping reads using STAR, fuse the count ouput for each sample to construct the count matrix
Installation
Index generation requires a big amount of RAM and time, therefore is best to perform it in OMICA. Install STAR2.6.1b through conda:
```
conda install -c bioconda star 
```
Download the required genome files from ENSEMBL FTP: [assembly.fasta](ftp://ftp.ensemblgenomes.org/pub/release-41/plants/fasta/vitis_vinifera/dna/) and [annotation.gtf](ftp://ftp.ensemblgenomes.org/pub/release-41/plants/gtf/vitis_vinifera)
```
wget ftp://ftp.ensemblgenomes.org/pub/release-41/plants/gtf/vitis_vinifera/Vitis_vinifera.12X.41.chr.gtf.gz
wget ftp://ftp.ensemblgenomes.org/pub/release-41/plants/fasta/vitis_vinifera/dna/Vitis_vinifera.12X.dna.toplevel.fa.gz
```
Uncompress
```
gunzip *.gz
```
Prepare the index through SLURM work administration in OMICA: star.slrm
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 48
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00

STAR --runThreadN 48 --runMode genomeGenerate --genomeDir ./ --genomeFastaFiles Vitis_vinifera.12X.dna.toplevel.fa --sjdbGTFfile Vitis_vinifera.12X.41.chr.gtf
```
Mapping of 1 sample through c22_star.slrm
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 20
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00

STAR --readFilesCommand zcat --runThreadN 20 --genomeDir ./genome_star_indexed/ --readFilesIn ../raw_grpvne_reads/c22_2.fastq.gz --outSAMtype BAM Unsorted --quantMode TranscriptomeSAM GeneCounts --outFileNamePrefix c22_2 --outReadsUnmappe
d Fastx 
```
Run mapping of mutiple files through multi_star.slrm
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 20
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00

for f in $(ls *[1-2].fastq.gz | sed 's/_[1-2].fastq.gz//' | sort -u); 
do STAR --readFilesCommand zcat --runThreadN 20 --genomeDir ../genome_star_indexed/ --readFilesIn ${f}_1.fastq.gz ${f}_2.fastq.gz --outSAMtype BAM Unsorted --quantMode TranscriptomeSAM GeneCounts --outFileNamePrefix ${f} --outReadsUnmapped Fastx;
done
```
Construct the count matrix in R (build_count_matrix.R)
```
ff <- list.files( path = "./counts", pattern = "*ReadsPerGene.out.tab$", full.names = TRUE )
counts.files <- lapply( ff, read.table, skip = 4 )
counts <- as.data.frame( sapply( counts.files, function(x) x[ , number ] ) )
ff <- gsub( "[.]ReadsPerGene[.]out[.]tab", "", ff )
ff <- gsub( "[.]/counts/", "", ff )
colnames(counts) <- ff
row.names(counts) <- counts.files[[1]]$V1
```
RESULT: BIG Variation between replicates when analyzing in edgeR, therefore I will try to increase the % of mapped reads using trimmomatic-trimmed reads (not raw because most of unmapped are flagged as too short). Using trimm.slrm:
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 1
#SBATCH -n 20
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00

for f in $(ls *fastq.gz | sed 's/_[1-2].fastq.gz//' | sort -u); do trimmomatic PE -threads 8 -basein ${f}_1.fastq.gz -baseout ./trimmomatic_out/${f}.fastq.gz ILLUMINACLIP:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/anaconda3/pkgs/trimmomatic-0.38-1/share/trimmomatic-0.38-1/adapters/TruSeq3-PE-2.fa:2:30:10 SLIDINGWINDOW:4:15 LEADING:5 TRAILING:5 MINLEN:25; done
```
Run the mapping in STAR but using the trimmed reads
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 1
#SBATCH -n 20
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00

for f in $(ls *[1-2]P.fastq.gz | sed 's/_[1-2]P.fastq.gz//' | sort -u); 
do STAR --readFilesCommand zcat --runThreads 20 --genomeDir ../../genome_star_indexed/ --readFilesIn ${f}_1P.fastq.gz ${f}_2P.fastq.gz --outSAMtype BAM Unsorted --quantMode TranscriptomeSAM GeneCounts --outFileNamePrefix ${f} --outReadsUnmapped Fastx;
done
```
RESULT: Same % of mapping that where obtained whn using raw sequence reads

###2)Evaluate gene expression and functional enrichment through edgeR-GOseq-GOplot pipeline
RESULT:There is high variability between replicates

##FUNGAL AND BACTERIAL IDENTIFICATION AND QUANTIFICATION
###1) Organelle and plant rRNA remotion (Despite the libraries were constructed using plant ribozero kit, still remain plant rRNA reads in output file)
Using the most representative sequence obtained in previous run MATAM output (rRNA from A. thaliana) as query in NCBI blast, I select the best 100 hits complete sequences and download it, then regions containing Ns were removed through sed and the resulting rRNA_NCBI.fasta file was used to filter out
Concatenate the rRNA sequences and organelle-derived sequences
```
cat rRNA_ncbi.fasta all_organelle_ADN_ARN.fasta > rRNA_organelle.fasta
```
Upload the concatenated file to OMICA

```
scp rRNA_organelle.fasta paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/bowtie2_out/
```
BOWTIE2 v2.3.4.3 is already installed in OMICA. Construct the index for organelles and ribosomal RNA and filter the unmapped reads on that index for c22 example through c22_rRNA_remot.slrm:
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 1
#SBATCH -n 6
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
#bowtie2-build rRNA_organelle.fasta rRNA_organelle
bowtie2 -p 6 -x rRNA_organelle -1 ../raw_grpvne_reads/trimmomatic_out/c22Unmapped.out.mate1 -2 ../raw_grpvne_reads/trimmomatic_out/c22Unmapped.out.mate2 | samtools view -bS -f 4 - | samtools fastq - > c22_rrna_org_unmap.fq
```
WITH ALL THE SAMPLES through rRNA_remot.slrm
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
#bowtie2-build rRNA_organelle.fasta rRNA_organelle
STARWD="../raw_grpvne_reads/trimmomatic_out/"
for f in $(ls $STARWD*Unmapped.out.mate[1-2] | cut -f4 -d"/" | sed 's/Unmapped.out.mate[1-2]//' | uniq); do bowtie2 -p 30 -x rRNA_organelle -1 $STARWD${f}Unmapped.out.mate1 -2 $STARWD${f}Unmapped.out.mate2 | samtools view -bS -f 4 - | sam
tools fastq - > ${f}_rrna_org_unmap.fq ; done
```
###2)Unmapped reads on grapevine genome (+ plant rRNA, + organelle sequences) needs to be processed on MATAM but using the enriched SILVA-SSU-DB with taxonomic edited classification
INSTALL MATAM in OMICA
```
conda install matam
```
Upload the enriched SILVA-SSU-DB with taxonomic edited classification
```
rsync -avP SILVA_132_SSURef_edit.fasta paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/raw_grpvne_reads/
```
Prepare MATAM reference through matam_prep.slrm
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00

matam_db_preprocessing.py -i SILVA_132_SSURef_edit.fasta --cpu 30 --max_memory 128000
```

Run MATAM assembly for c22 through c22_matam.slrm
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
matam_assembly.py -i c22_rrna_org_unmap.fq -d SILVA_132_SSURef_edit_NR95 -o ./c22_matam_norrna_nogrpvne_out/ -v --cpu 30 --max_memory 128000 --perform_taxonomic_assignment --min_scaffold_length 200
```
Run MATAM assembly for all the samples through matam.slrm
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
BWTIEDIR="../bowtie2_out/"
for f in $(ls $BWTIEDIR*_rrna_org_unmap.fq | cut -f3 -d"/" | sed 's/_rrna_org_unmap.fq//g'); do matam_assembly.py -i $BWTIEDIR${f}_rrna_org_unmap.fq -d SILVA_132_SSURef_edit_NR95 -o ./${f}_matam_norrna_nogrpvne_out/ -v --cpu 30 --max_memory 128000 --perform_taxonomic_assignment --min_scaffold_length 200; done
```
Download all the krona files from OMICA:
```
for f in c22 c23 c25 m11 m16 m26; do scp paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/matam_out/${f}_matam_norrna_nogrpvne_out/krona.html ./de_OMICA/${f}_krona.html; done
```
Construct the table for comparisons between samples
```
1)First download the scaffolds assembly and rdp classification from OMICA
for d in "c22" "c23" "c25" "m11" "m16" "m26"; do scp paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/matam_out/${f}_matam_norrna_nogrpvne_out/final_assembly.fa ./${d}_final_assembly.fa; done
for d in "c22" "c23" "c25" "m11" "m16" "m26"; do scp paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/matam_out/${f}_matam_norrna_nogrpvne_out/rdp.tab ./${d}_rdp.tab; done
2) construir el archivo para indicar paths de muestras para archivos fasta y rdp, terminar de editar detalles con nano
for d in "c22" "c23" "c25" "m11" "m16" "m26"; do echo $d $d*{fa,tab} | sed 's/ /\t/g' >> samples_file; done
3) Correr comparacion con matam
matam_compare_samples.py -s samples_file -t abundance_table.txt -c taxonomy_table.txt
```
For OTU abundance and TAxonomy files contruction that could be compatible with phyloseq R script:
```
for q in "c22" "c23" "c25" "m11" "m16" "m26"; do grep "$q" abundance_table.txt | cut -f1-4 > ${q}_raw_abun; done
# then the sum of all the counts per sample is required:
for f in "c22" "c23" "c25" "m11" "m16" "m26"; do cut -f3 ${f}_raw_abun | awk '{sum+=$1} END {print sum}'; done
#RESULT
#c22 110995
#c23 199276
#c25 161955
#m11 125936
#m16 141967
#m26 156236
#finally is required to modify the taxonomy_table.txt file to express the values of raw abunance instead of normalized values and separate the taxonomy file from otu table files
sed 's/None/0.00001/g' taxonomy_table.txt > tax_table_with0.txt
gawk -F$'\t' 'BEGIN {OFS = FS} $2*=1109.95' tax_table_with0.txt | gawk -F$'\t' 'BEGIN {OFS = FS} $3*=1992.76' | gawk -F$'\t' 'BEGIN {OFS = FS} $4*=1619.55' | gawk -F$'\t' 'BEGIN {OFS = FS} $5*=1259.36' | gawk -F$'\t' 'BEGIN {OFS = FS} $6*=1419.67' | gawk -F$'\t' 'BEGIN {OFS = FS} $7*=1562.36' > tax_table_raw_abun.txt
sed -ne '/^/{=;p}' tax_table_raw_abun.txt | sed  '/^[0-9]/N;s/\n/_OTU\t/' | cut -f1,3-8 > otu_table_4_phyloseq.tab
sed -ne '/^/{=;p}' tax_table_raw_abun.txt | sed  '/^[0-9]/N;s/\n/_OTU\t/' | cut -f1-2 |  sed -s 's/;/\t/g' > tax_table_4_phyloseq.tab 
#final editing with nano to include ("otu	Domain	Supergroup	Division	Class	Order Family	Genus	Species") as header in tax file and ("otu	c22	c23	c25	m11	m16	m26") as header in otu file
```
RESULTS: all the taxonomic assignment corresponds to bacteria and in minor proportion to archaeae(0.03%) but there is no assignment for Eukaryota. Checking using SSU_DB but with fungal model is required to rescue the SSU DB.
Run MATAM assembly for c22 through c22_matam_fungalSSU.slrm
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
matam_assembly.py -i c22_rrna_org_unmap.fq -d SILVA_132_SSURef_edit_NR95 -o ./c22_matam_norrna_nogrpvne_out/ -v --cpu 30 --max_memory 128000 --perform_taxonomic_assignment --min_scaffold_length 200 --training_model fungallsu
```
RESULTS: Only classiffy for metazoa, so I need to enrich the SILVA_LSU DB, and then use the fungallsu model and this DB to assign taxonomy.

###SILVA_LSU DB enrichment with sequences of well-known GTD-associated fungi
From sequeneces of GTD pathogens downloaded from ENA, dereplicate, remove "N/Ns" containing sequences and those having "uncultured/unverified" flags (through nano)  
```
/home/quetjaune/cdhit/cd-hit-est -M 8000 -i grpvne_pathogens_ena_dwnload_onlyLSUseqs.fasta -o grpvne_path_enadwnld_LSU_uniq.fasta
```
Extract species/genus names for each organism in SILVA_132_LSURef fasta header
```
grep ">" SILVA_132_LSURef_tax_silva.fasta | sed 's/\..*;/\t/' | sed 's/>//' > accnumber_SILVALSU132_to_specname
```
Identify those that are flagged as unidentified (595seqs), delete those from accnumber file and retain only the species name from each entry; then fuse with GTD_pathogens_sequences
```
grep unidentified accnumber_SILVALSU132_to_specname > accnumber_SILVALSU132_to_unidentified
sed -i '/unidentified/d' accnumber_SILVALSU132_to_specname
cut -f2 accnumber_SILVALSU132_to_specname > specname_from_SILVA
```
Get the NCBI full and updated taxonomic lineage using the species names and taxid files 
```
sort -u specname_from_SILVA > specname_from_SILVA_uniq 
cat specname_from_SILVA_uniq | xargs -I{} grep -m 1 -w {} ../matam_SILVA_SSU_DB/fullnamelineage.dmp > specname_from_SILVA_ncbi_lineage
```
Some editing is required to match ncbi lineage files with the format expected by SILVA, then fuse the lineages files both from SILVA references and from GTD_pathogens
```
paste <(cut -f5 specname_from_SILVA_ncbi_lineage) <(cut -f3 specname_from_SILVA_ncbi_lineage) | cut -f3-12 -d " " | sed 's/; /;/g;s/\t//g' | sort -u > specname_SILVA_ncbi_lineag_reord
sed -i 's/\t/ /' accnumber_SILVALSU132_to_specname 
bash fusion_files.sh | sed '/ NA/d' > accnumber_SILVALSU132_to_lineage
cat accnumber_GTDpath_to_lineage_final accnumber_SILVALSU132_to_lineage > accnumber_SILVALSU132_GTD_path_to_lineage
```
Also the fasta header needs some modifications (remove duplicates, remove sequences of unidentified species and change the accnumber_ID(without the number after first dot)
```
sed 's/\.[0-9]*\.[0-9]*//' SILVA_132_LSURef_tax_silva.fasta > SILVA_132_LSURef_tax_silva_shortID.fasta
seqkit rmdup -n SILVA_132_LSURef_tax_silva_shortID.fasta > SILVA_132_LSURef_tax_silva_shortID_nodup.fasta
seqkit grep -f <(cut -f1 accnumber_SILVALSU132_to_specname) SILVA_132_LSURef_tax_silva_shortID_nodup.fasta > SILVA_132_LSURef_tax_silva_nodup_nounident.fasta
seqkit grep -f <(cut -f1 -d" " accnumber_SILVALSU132_to_lineage) SILVA_132_LSURef_tax_silva_nodup_nounident.fasta > SILVA_132_LSURef_tax_silva_to_replace.fasta
```
Finally include the GTD_pathoges fasta sequences in SILVA_LSU DB fasta sequences, and edit to replace the headers. The final output is **SILVA_132_LSURef_GTDpath.fasta**
```
cat grpvne_pathogens_ena_dwnload_derep_newhead.fasta SILVA_132_LSURef_tax_silva_to_replace.fasta > SILVA_132_LSURef_edit.fasta
python fasta_head_replace.py
```
Then I upload the resulting file to OMICA
```
rsync -avP SILVA_132_LSURef_GTDpath.fasta paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/matam_out/
```
Prepare DB for matam (as before wirh SSURef) and run for c22:
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
matam_assembly.py -i c22_rrna_org_unmap.fq -d SILVA_132_LSURef_GTDpath_NR95 -o ./c22_fungalSSU_matam_norrna_nogrpvne_out/ -v --cpu 30 --max_memory 128000 --perform_taxonomic_assignment --min_scaffold_length 200 --training_model fungallsu
```
Then run for all the samples
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
BWTIEDIR="../bowtie2_out/"
for f in $(ls $BWTIEDIR*_rrna_org_unmap.fq | cut -f3 -d"/" | sed 's/_rrna_org_unmap.fq//g'); do matam_assembly.py -i $BWTIEDIR${f}_rrna_org_unmap.fq -d SILVA_132_LSURef_GTDpath_NR95 -o ./${f}_fungalLSU_norrna_nogrpvne_out/ -v --cpu 30 --max_memory 128000 --perform_taxonomic_assignment --min_scaffold_length 200 --training_model fungallsu; done
```
Download krona files from OMICA
```
scp paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/matam_out/c22_fungalLSU_norrna_nogrpvne_out/krona.html ./c22_fungalLSU_krona.html
```
For comparisons between samples is required toto download **rdp** and **fasta** files for each sample and run matam_compare_samples.py (as explained for SSU). Then is required to modify **abundance_table.txt** to be compatible with phyloseq required input in **phyloseq_graphs_matam_euk.R** script
```
for q in "c22" "c23" "c25" "m11" "m16" "m26"; do grep "$q" abundance_table.txt | cut -f1-4 > ${q}_raw_abun; done
# then the sum of all the counts per sample is required:
for f in "c22" "c23" "c25" "m11" "m16" "m26"; do cut -f3 ${f}_raw_abun | awk '{sum+=$1} END {print sum}'; done
#RESULT
#c22 1227215
#c23 4086625
#c25 1452667
#m11 1741677
#m16 1525207
#m26 756725
#finally is required to modify the taxonomy_table.txt file to express the values of raw abunance instead of normalized values and separate the taxonomy file from otu table files
sed 's/None/0.00001/g' taxonomy_table.txt > tax_table_with0.txt
gawk -F$'\t' 'BEGIN {OFS = FS} $2*=12272.15' tax_table_with0.txt | gawk -F$'\t' 'BEGIN {OFS = FS} $3*=40866.25' | gawk -F$'\t' 'BEGIN {OFS = FS} $4*=14526.67' | gawk -F$'\t' 'BEGIN {OFS = FS} $5*=17416.77' | gawk -F$'\t' 'BEGIN {OFS = FS} $6*=15252.07' | gawk -F$'\t' 'BEGIN {OFS = FS} $7*=7567.25' > tax_table_raw_abun.txt
sed -ne '/^/{=;p}' tax_table_raw_abun.txt | sed  '/^[0-9]/N;s/\n/_OTU\t/' | cut -f1,3-8 > otu_table_4_phyloseq.tab
sed -ne '/^/{=;p}' tax_table_raw_abun.txt | sed  '/^[0-9]/N;s/\n/_OTU\t/' | cut -f1-2 |  sed -s 's/;/\t/g' | sed 's/ /_/g' > tax_table_4_phyloseq.tab 
#final editing with nano to include ("otu     Superphylum     Phylum  Class   Order Family    Genus") as header in tax file and ("otu	c22	c23	c25	m11	m16	m26") as header in otu file
```


MATAM was designed to analyze metagenomic data therefore is expected to have lower redundancy than in metratranscriptomic data, therefore is intenteresting to evaluate MATAM performance with dereplicated data (clustering at 95%) 
Start using dereplicating C22 fastq files. Install first cd-hit v4.7 through conda:
```
conda install -c bioconda cd-hit 
```
Using cdhit_c22.slrm:
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
cd-hit-est -M 128000 -T 30 -c 0.95 -i c22_rrna_org_unmap.fq -o derep_cdhit_out/c22_rrna_org_unmap_NR95.fq
```
then run matam but using dereplicated sequences through c22_matam_fungalLSU_derep.slrm:
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
matam_assembly.py -i ../bowtie2_out/derep_cdhit_out/c22_rrna_org_unmap_NR95.fq -d SILVA_132_LSURef_GTDpath_NR95 -o ./c22_fungalSSU_matam_norrna_nogrpvne_out/ -v --cpu 30 --max_memory 128000 --perform_taxonomic_assignment --min_scaffold_length 200 --training_model fungallsu
```
Now run dereplicate (x cdhit.slrm) for all the samples
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
#bowtie2-build rRNA_organelle.fasta rRNA_organelle
for f in $(ls *rrna_org_unmap.fq | sed 's/_rrna_org_unmap.fq//' | uniq); do cd-hit-est -M 128000 -T 30 -c 0.95 -i ${f}_rrna_org_unmap.fq -o derep_cdhit_out/${f}_rrna_org_unmap_NR95.fq ; done
```
And run matam for all the samples (matam_derep.slrm)
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
for f in $(ls ../bowtie2_out/derep_cdhit_out/*NR95.fq | cut -f4 -d"/" | sed 's/_rrna_org_unmap_NR95.fq//' | uniq); do matam_assembly.py -i ${f}_rrna_org_unmap_NR95.fq -d SILVA_132_LSURef_GTDpath_NR95 -o ./{f}_fungalLSU_norrna_nogrpvne_derep_matam_out/ -v --cpu 30 --max_memory 128000 --perform_taxonomic_assignment --min_scaffold_length 200 --training_model fungallsu; done
```
Download krona files from OMICA
```
scp paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/matam_out/c22_fungalSSU_matam_norrna_nogrpvne_out/krona.html ./c22_fungalLSU_derep_krona.html
```
RESULT: Its clear that the assembly is better (longer and higher final assembled sequences) when using all the reads (without dereplication) but there are some sequences that did not appear in the final krona file in the different treatments (a.i.e Phaeoacremonium)
I will try downloading the scaffolds_min200pb fasta file obtained for each sample (the undereplicated one) to concatenate for all the samples, then dereplicate, get the best blast hit (using SILVA db as reference) and with the taxonomica defined assembled sequences will be used as reference for mapping the reads through bowtie2


###3)Construct matrix for comparison between symptomatic vs asymptomatic plants. Quantify statistical differences.
COncatenate the scaffolds obtained for each sample
```
cat *_fungalLSU_norrna_nogrpvne_out/workdir/scaffolds.NR.min_200bp.fa > cat_scaffolds_fungalLSU.fa
```
Use cdhit to dereplicate based on homology (100%) through cdhit_scaff.slrm
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
cd-hit-est -M 128000 -T 30 -c 1 -i cat_scaffolds_fungalLSU.fa -o cat_scaffolds_fungalLSU_NR100.fa
```
REMOVE DUPLICATES FROM SILVA_132_LSURef_GTDpath.fasta (I don't know why I have duplicates here)
```
seqkit rmdup -n SILVA_132_LSURef_GTDpath.fasta > SILVA_132_LSURef_GTDpath_nodup.fasta
```
RUN BLASTN through **blastn_concat_SILVA132GTDpath.slrm** with the common assembled sequences in MATAM
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
makeblastdb -in SILVA_132_LSURef_GTDpath_nodup.fasta -dbtype nucl 
blastn -query cat_scaffolds_fungalLSU_NR100.fa -db SILVA_132_LSURef_GTDpath_nodup.fasta -outfmt 6 -num_threads 30 -max_target_seqs 1 -evalue 1e-15 -perc_identity 97 -out cat_scaffolds_fungalLSU_NR100.SILVA132LSUGTD_iden97.tab
```
Extract the unique hit IDs from blastn output and get a fasta only having the Matam assembled sequences
```
cut -f2 cat_scaffolds_fungalLSU_NR100.SILVA132LSUGTD_iden97.tab | sort -u > uniq_ID_blastn_matamassem_SILVALSU132GTDpath
seqkit grep -f uniq_ID_blastn_matamassem_SILVALSU132GTDpath SILVA_132_LSURef_GTDpath_nodup.fasta > SILVA_132_LSURef_GTDpath_nodup_matamassem.fasta
```
Map the sequences for each sample using SILVA_132_LSURef_GTDpath_nodup_matamassem.fasta as reference in **bowtie2_matass.slrm**
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 1
#SBATCH -n 6
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
bowtie2-build SILVA_132_LSURef_GTDpath_nodup_matamassem.fasta LSUref_GTDpath_matamassem
BWTIEDIR="../bowtie2_out/"
for f in $(ls $BWTIEDIR*_rrna_org_unmap.fq | cut -f3 -d"/" | sed 's/_rrna_org_unmap.fq//g'); 
do bowtie2 --threads 6 -x LSUref_GTDpath_matamassem -U $BWTIEDIR${f}_rrna_org_unmap.fq | samtools view -Sb - | samtools sort - > ${f}_matassem_sorted.bam;
done
```
Count the mapped reads through **count_matass.slrm**
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 1
#SBATCH -n 6
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
for f in $(ls *_matassem_sorted.bam | sed 's/_matassem_sorted.bam//'); 
do samtools view ${f}_matassem_sorted.bam | perl ../count_names.pl > ${f}_matassem.stats; 
done
```
DOwnloads the count files from OMICA
```
scp paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/matam_out/<name_file>_matassem.stats ./<name_file>_matassem.stats
```
Construct the table counts and calculate statistical differences through **8_build_count_table.R** and **9_differential_diversity_analysis**
RESULT: high variability. Try again but using all the SILVA_LSURef concatenated to our DB_GTDpath

RESULT: A similar result than before was obtained, I do not know why does I have "Camarosporium" when the sequences were eliminated in SILVA_132_LSURef_GTDpath.fasta, I will try dereplicating the fasta reference, clustering at 100%, 
remove sequences containing > 2 N's; with min length=200
**matam_prep.slrm**
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00

matam_db_preprocessing.py -i SILVA_132_LSURef_GTDpath_nodup.fasta -m 200 -n 2 --clustering_id_threshold 1 --cpu 30 --max_memory 128000 --verbose
```
Then run assembly for all the samples
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
BWTIEDIR="../bowtie2_out/"
for f in $(ls $BWTIEDIR*_rrna_org_unmap.fq | cut -f3 -d"/" | sed 's/_rrna_org_unmap.fq//g'); 
do matam_assembly.py -i $BWTIEDIR${f}_rrna_org_unmap.fq -d SILVA_132_LSURef_GTDpath_nodup_NR100 -o ./${f}_fungalLSUNR100_norrna_nogrpvne_out/ -v --cpu 30 --max_memory 128000 --perform_taxonomic_assignment \
--min_scaffold_length 200 --training_model fungallsu;
done
```

On other side, I will analyze through blastn but using all the assembled sequences (instead of only those larger than 200 bp)
```
cat *_fungalLSU_norrna_nogrpvne_out/workdir/scaffolds*.fa > cat_scaffolds_fungalLSU.fa
```
Run **blastn_concat_SILVA132GTDpath.slrm** and
```
cut -f2 cat_scaffolds_fungalLSU_NR100.SILVA132LSUGTD_iden97.tab | sort -u > uniq_ID_blastn_matamassem_SILVALSU132GTDpath
seqkit grep -f uniq_ID_blastn_matamassem_SILVALSU132GTDpath SILVA_132_LSURef_GTDpath_nodup.fasta > SILVA_132_LSURef_GTDpath_nodup_matamassem.fasta
```
and **bowtie2_matass.slrm** and finally **count_matass.slrm**
RESULT: same as before :-(..expect to found better results using SILVAcluster100 but the same again

###Let's try using UNITE_ITS DB
After downloading **UNITEv6_sh_dynamic.fasta** and **UNITEv6_sh_dynamic.tax** (from mothur, but directly from [UNITE](https://unite.ut.ee/sh_files/sh_general_release_10.10.2017.zip) seems better) get fused file through python custom script **fasta_head_replace.py**:
```
# -*- coding: utf-8 -*-
#!/usr/bin/env python
fasta= open('UNITEv6_sh_dynamic.fasta')
newnames= open('UNITEv6_sh_dynamic.tax')
newfasta= open('UNITEv6_sh_dynamic_tax.fasta', 'w')

for line in fasta:
    if line.startswith('>'):
        newname= '>'+newnames.readline()
        newfasta.write(newname)
    else:
        newfasta.write(line)

fasta.close()
newnames.close()
newfasta.close()
```
Alternative:
Download directly from [UNITE](https://unite.ut.ee/sh_files/sh_general_release_10.10.2017.zip) and uncompress
```
wget -c https://unite.ut.ee/sh_files/sh_general_release_10.10.2017.zip
unzip sh_general_release_10.10.2017.zip
```
then prepare DB with **matam_prep.slrm** and calculate fungal diversity abundance with **matam_uniteITS.skrm**


With the resulting file **UNITEv6_sh_dynamic_tax.fasta** the matam db preprocessing through **matam_prep.slrm** needs to be run:
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
matam_db_preprocessing.py -i UNITEv6_sh_dynamic_tax.fasta -m 200 -n 2 --clustering_id_threshold 0.97 --cpu 30 --max_memory 128000 --verbose
```
then run matam assembly through **matam_uniteITS.slrm** 
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
BWTIEDIR="/LUSTRE/bioinformatica_data/genomica_funcional/paoline/bowtie2_out/"
for f in $(ls $BWTIEDIR*_rrna_org_unmap.fq | cut -f7 -d"/" | sed 's/_rrna_org_unmap.fq//g'); 
do matam_assembly.py -i $BWTIEDIR${f}_rrna_org_unmap.fq -d UNITEv6_sh_dynamic_tax_NR97 -o ./${f}_procSSU_norrna_nogrpvne_out/ -v --cpu 30 --max_memory 128000 --perform_taxonomic_assignment \
--min_scaffold_length 200 --training_model fungalits_unite; 
done
```
RESULTS: Stuck when assign taxonomic data to the assembled scaffolds. Requires to modify the input fasta file:I have to use a [rdp.patch.txt](https://github.com/bonsai-team/matam/files/2772568/rdp.patch.txt) to modify the rdp.py, that allow the inclusion of species (7 taxonomics rank) when assigning taxonomies

Run again **matam_prep.slrm** and **matam_uniteITS.slrm*

###Also I would explore using [WARCUP_DB](https://www.drive5.com/sintax/rdp_its_v2.fa.gz). Also need to use the rdp.patch in rdp.py to allow the classification
**matam_prep.slrm**
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
matam_db_preprocessing.py -i rdp_its_v2.fa -m 200 -n 2 --clustering_id_threshold 0.97 --cpu 30 --max_memory 128000 --verbose
```
**matam_rdpITS.slrm**
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 30
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
BWTIEDIR="/LUSTRE/bioinformatica_data/genomica_funcional/paoline/bowtie2_out/"
for f in $(ls $BWTIEDIR*_rrna_org_unmap.fq | cut -f7 -d"/" | sed 's/_rrna_org_unmap.fq//g'); 
do matam_assembly.py -i $BWTIEDIR${f}_rrna_org_unmap.fq -d rdp_its_v2_NR97 -o ./${f}_RDPITS_norrna_nogrpvne_out/ -v --cpu 30 --max_memory 128000 --perform_taxonomic_assignment --min_scaffold_length 200 --training_model fungalits_warcup; 
done
```
Download krona.html files from OMICA
RESULTS: REALLY confident results in terms of taxonomic resolution

For Sample comparison, is required to join in a folder in OMICA the **rdp** and **fasta** files and then run **matam_compare_samples.py**
```
1) construir el archivo para indicar paths de muestras para archivos fasta y rdp, terminar de editar detalles con nano
for d in "c22" "c23" "c25" "m11" "m16" "m26"; do echo $d $d*{fa,tab} | sed 's/ /\t/g' >> samples_file; done
2) Correr comparacion con matam
matam_compare_samples.py -s samples_file -t abundance_table.txt -c taxonomy_table.txt
3)Download output files from OMICA
scp paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/matam_out/rdp_warcup_db/matam_sampple_comparison/abundance_table.txt ./
scp paoline@omica:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/matam_out/rdp_warcup_db/matam_sampple_comparison/taxonomy_table.txt ./
4) Sum the total counts per sample
for q in "c22" "c23" "c25" "m11" "m16" "m26"; do grep "$q" abundance_table.txt | cut -f1-4 > ${q}_raw_abun; done
# then the sum of all the counts per sample is required:
for f in "c22" "c23" "c25" "m11" "m16" "m26"; do cut -f3 ${f}_raw_abun | awk '{sum+=$1} END {print sum}'; done
#RESULT
#c22 1099
#c23 3622
#c25 1302
#m11 1600
#m16 1337
#m26 965
#finally is required to modify the taxonomy_table.txt file to express the values of raw abunance instead of normalized values and separate the taxonomy file from otu table files
sed 's/None/0.00001/g' taxonomy_table.txt > tax_table_with0.txt
gawk -F$'\t' 'BEGIN {OFS = FS} $2*=10.99' table_with0.txt | gawk -F$'\t' 'BEGIN {OFS = FS} $3*=36.22' | gawk -F$'\t' 'BEGIN {OFS = FS} $4*=13.02' | gawk -F$'\t' 'BEGIN {OFS = FS} $5*=16' | gawk -F$'\t' 'BEGIN {OFS = FS} $6*=13.37' | gawk -F$'\t' 'BEGIN {OFS = FS} $7*=9.65' > tax_table_raw_abun.txt
sed -ne '/^/{=;p}' tax_table_raw_abun.txt | sed  '/^[0-9]/N;s/\n/_OTU\t/' | cut -f1,3-8 > otu_table_4_phyloseq.tab
sed -ne '/^/{=;p}' tax_table_raw_abun.txt | sed  '/^[0-9]/N;s/\n/_OTU\t/' | cut -f1-2 |  sed -s 's/;/\t/g' | sed 's/ /_/g' > tax_table_4_phyloseq.tab 
#final editing with nano to include ("otu     Superphylum     Phylum  Class   Order Family    Genus	Species") as header in tax file and ("otu	c22	c23	c25	m11	m16	m26") as header in otu file
```

Let's try fungal 18S DB (adapted from SILVA-SSU but specialized in Fungi, check: Yarza et al., 2017):..RESULT: Not working because of incorrect fasta format.
##USING KRAKENUNIQ

Alternative for assigning taxonomic information to reads is through [krakenuniq](https://github.com/fbreitwieser/krakenuniq) that uses all the reads, not only the ribosomal RNA reads (like in MATAM)
Download the indexed database containing all the NCBI genomes from microbes (archeas, bacteria, virus and fungi) in NT DB
```
wget -c ftp://ftp.ccb.jhu.edu/pub/software/krakenuniq/Databases/nt/*
```
Then run kraken read classification through **kraken_classif.slrm**
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 1
#SBATCH -n 12
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
#krakenuniq-build --db microb_nt_db --kmer-len 31 --threads 10 --taxids-for-genomes --taxids-for-sequences --jellyfish-hash-size 6400M
krakenuniq --db microb_nt_db --fastq-input ../bowtie2_out/c22_rrna_org_unmap.fq --threads 12 --report-file REPORTFILE.tsv > READCLASSIFICATION.tsv
```
RESULTS: really comprehensive taxonomic classification and relative abundance. Coincide mainly with MATAM-warcup-ITS classification for fungi and MATAM-SILVA-SSU for bacteria in the most abundant genus
Then to contruct Krona graphs from Krakenuniq output (REPORTFILE.tsv) the next command was used for each sample:
```
ktImportTaxonomy -o m26_s6_kraken.krona.html -t 7 -s 6 REPORTFILE.tsv 
```
First we have to update all the files regarding taxonomy or accesions IDs through **updateTaxonomy.sh** and **updateAccession.sh?**

Then I would like to evaluate the microbial functional profiling therefore I would try thorugh [FMAP](https://github.com/jiwoongbio/FMAP) and/or [MEGAHIT](https://github.com/voutcn/megahit) and/or [Humann2](https://bitbucket.org/biobakery/humann2/wiki/Home) 
and/or [FishTaco](http://borenstein-lab.github.io/fishtaco/) and/or [kaiju](https://github.com/bioinformatics-centre/kaiju)

**[Humann2](https://bitbucket.org/biobakery/humann2/wiki/Home)**
Create environment **humann** (because of python2.7 requirement), Install through conda and activate first to run in OMICA
```
conda create -n humann python=2.7 anaconda
conda install -c bioconda humann2
source activate humann
```

humann2.slrm for c22 sample
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 1
##SBATCH -n 12
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
##export PATH=$PATH:/LUSTRE/bioinformatica_data/genomica_funcional/paoline/anaconda3/envs/humann/bin/
##METAPHLAN_DB=/LUSTRE/bioinformatica_data/genomica_funcional/paoline/anaconda3/envs/humann/bin/databases
humann2 --input ../bowtie2_out/c22_rrna_org_unmap.fq --bypass-prescreen --output c22
```
humann2 for all the samples
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 12
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
BWTIEDIR="/LUSTRE/bioinformatica_data/genomica_funcional/paoline/bowtie2_out/"
for f in $(ls $BWTIEDIR*_rrna_org_unmap.fq | cut -f7 -d"/" | sed 's/_rrna_org_unmap.fq//g'); 
do humann2 --input ../bowtie2_out/$f_rrna_org_unmap.fq --bypass-prescreen --threads 12 --output $f;
done
```
Alternatively we could run [metaphlan](https://bitbucket.org/biobakery/metaphlan2/) first, to identify the taxonomic profile (based only on marker genes) for all the samples, 
and then use the list of identified microorganisms to continue with humann2 pipeline:
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 1
#SBATCH -n 12
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
BWTIEDIR="/LUSTRE/bioinformatica_data/genomica_funcional/paoline/bowtie2_out/"
for f in $(ls $BWTIEDIR[cm][1,2][1,3,5,6]_rrna_org_unmap.fq | cut -f7 -d"/" | sed 's/_rrna_org_unmap.fq//g'); 
do metaphlan2.py $BWTIEDIR${f}_rrna_org_unmap.fq --mpa_pkl /LUSTRE/bioinformatica_data/genomica_funcional/paoline/anaconda3/envs/humann/bin/databases/mpa_v20_m200.pkl --bowtie2db /LUSTRE/bioinformatica_data/genomica_funcional/paoline/anac
onda3/envs/humann/bin/databases/ --nproc 12 --input_type fastq > ${f}_profiled_metagenome.txt;
done
```
And then run humann2
```
#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 2
#SBATCH -n 12
### Cantidad de tiempo max de ejecucion (formato extendido o minutos)
#SBATCH -t 6-00:00:00
BWTIEDIR="/LUSTRE/bioinformatica_data/genomica_funcional/paoline/bowtie2_out/"
for f in $(ls $BWTIEDIR*_rrna_org_unmap.fq | cut -f7 -d"/" | sed 's/_rrna_org_unmap.fq//g'); 
do humann2 --input ../bowtie2_out/${f}_rrna_org_unmap.fq --taxonomic-profile ${f}_profiled_metagenome.txt --threads 12 --output ${f};
done
```
join tables of each sample and graph barplot images
```
humann2_join_tables -i ./ -o all_join_tables.tsv
humann2_barplot -i control_join_tables_RPK.tsv -f UniRef50_A0A009PBA3 -o controls_A0A009PBA3
```
Normalize counts (make the data independent of the bias introduced by different sequencing deep and allow the correct sample comparison), regroup based on EC codes (from KEGG)
```
humann2_renorm_table --input all_join_tables_RPK.tsv --output all_join_genefamilies_RPKs_cpm.tsv --units cpm --update-snames
humann2_regroup_table -i all_join_genefamilies_RPKs_cpm.tsv -o all_join_level4ec-cpm.tsv -g uniref50_rxn
```
Make statistical analysis (Kluscal-Wallis) after modifying **all_join_level4ec-cpm.tsv** file with a new row that indicates the symptomatic and asymptomatic samples 
```
humann2_associate --input all_join_level4ec-cpm.tsv --last-metadatum SYMPT --focal-metadatum SYMPT --focal-type categorical -o stats_SYM_vs_ASYM.txt -f 0.5
```
Download the Uniref90 full db (recomended) and update configuration
```
humann2_config --update database_folders protein /LUSTRE/bioinformatica_data/genomica_funcional/paoline/humman2_out/uniref90
```


##INTEGRATIVE APPROACH (based on Rodrigues et al., 2018 TranNetwork)

###Mixed matrix (counts on genes/transcripts plus counts on organism (fungi + bacteria + virus?)
###Statistical analysis that correlate regulated genes or GO terms with relative quntities of each identified microorganism

##VIRUS IDENTIFICATION AND QUANTIFICATION
###1)With clean reads (Trimmomatic output) make de novo assembly using Trinity or following this [protocol](https://www.nature.com/articles/srep23774)
###2)Identify already known virus based on blastn/blastx hits
###3)Identify new virus based on phylogenetics analysis [protocol](https://www.nature.com/articles/srep23774)
