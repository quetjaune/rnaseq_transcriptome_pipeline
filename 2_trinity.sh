#!/bin/sh
#SBATCH -p cicese
#SBATCH -N 1
#SBATCH -n 24
#SBATCH -J trinity

TRINITYHOME=/LUSTRE/bioinformatica_data/genomica_funcional/bin/trinityrnaseq
export $TRINITYHOME

$TRINITYHOME/Trinity --seqType fq --max_memory 4G --left reads.ALL.left.fq --right reads.ALL.right.fq --CPU 24